//
//  ObservingLabel.swift
//  teachat3
//
//  Created by Julio Alorro on 10/17/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

class ObservingLabel: UILabel {

    override var isEnabled: Bool {
        didSet {
            switch isEnabled {
                case true:
                    self.alpha = 1.0
            
                case false:
                    self.alpha = 0.8
            }
        }
    }

}
