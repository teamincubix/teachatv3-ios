//
//  RegisterVCTests.swift
//  teachat3
//
//  Created by Julio Alorro on 11/11/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import XCTest
@testable import teachat3

class RegisterVCTests: XCTestCase {
    
    fileprivate var vc: RegisterVC!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vc = RegisterVC()
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    // MARK: - RegisterVC asynchronous networking to Subject Endpoint
    func testSubjectEndpoint() {
        vc.viewDidLoad()
        
        let someExpectation = expectation(description: "Querying Subject Endpoint")
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) { [unowned self] in
            XCTAssertFalse(self.vc.countryTextField.text!.isEmpty)
            XCTAssertFalse(self.vc.stateTextField.text!.isEmpty)
            XCTAssertFalse(self.vc.schoolTextField.text!.isEmpty)
            someExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
    }
    // MARK: - RegisterVC IBAction of hiding/unhiding of teacher related textfields
    func testRoleSegmentedControl() {
        
        vc.roleSegmentControl.selectedSegmentIndex = 0
        vc.roleControlPressed()
        
        XCTAssertFalse(vc.countryTextField.isHidden)
        XCTAssertFalse(vc.stateTextField.isHidden)
        XCTAssertFalse(vc.schoolTextField.isHidden)
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.roleControlPressed()
        
        XCTAssertTrue(vc.countryTextField.isHidden)
        XCTAssertTrue(vc.stateTextField.isHidden)
        XCTAssertTrue(vc.schoolTextField.isHidden)
        
    }
    
    // MARK: - RegisterVC TextField Conditionals
    func testMissingInfoInTextFields() {
        let error = TeachatError.missingInput(RegisterVC.ErrorText.missing.rawValue)
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.titleSegmentControl.selectedSegmentIndex = 0
        vc.firstNameTextField.text = "Some"
        vc.lastNameTextField.text = "Guy"
        vc.emailTextField.text = ""
        vc.passwordTextField.text = "somePassword"
        vc.confirmPasswordTextField.text = "somePassword"
        
        vc.registerButtonPressed()
        
        guard let alert = vc.presentedViewController as? UIAlertController else {
            XCTFail(); return
        }
        
        XCTAssertEqual(alert.title, error.title)
        XCTAssertEqual(alert.message, error.localizedDescription)
    }
    
    func testInvalidEmail() {
        let error = TeachatError.invalidInput(RegisterVC.ErrorText.email.rawValue)
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.titleSegmentControl.selectedSegmentIndex = 0
        vc.firstNameTextField.text = "Some"
        vc.lastNameTextField.text = "Guy"
        vc.emailTextField.text = "notAnEmail"
        vc.passwordTextField.text = "somePassword"
        vc.confirmPasswordTextField.text = vc.passwordTextField.text!
        
        vc.registerButtonPressed()
        
        guard let alert = vc.presentedViewController as? UIAlertController else {
            XCTFail(); return
        }
        
        XCTAssertEqual(alert.title, error.title)
        XCTAssertEqual(alert.message, error.localizedDescription)
    }
    
    func testPasswordMismatch() {
        let error = TeachatError.invalidInput(RegisterVC.ErrorText.mismatch.rawValue)
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.titleSegmentControl.selectedSegmentIndex = 0
        vc.firstNameTextField.text = "Some"
        vc.lastNameTextField.text = "Guy"
        vc.emailTextField.text = "thisIs@valid.com"
        vc.passwordTextField.text = "somePassword"
        vc.confirmPasswordTextField.text = "anotherPassword"
        
        vc.registerButtonPressed()
        
        guard let alert = vc.presentedViewController as? UIAlertController else {
            XCTFail(); return
        }
        
        XCTAssertEqual(alert.title, error.title)
        XCTAssertEqual(alert.message, error.localizedDescription)
    }
    
    func testPasswordTooShort() {
    
        let error = TeachatError.invalidInput(RegisterVC.ErrorText.short.rawValue)
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.titleSegmentControl.selectedSegmentIndex = 0
        vc.firstNameTextField.text = "Some"
        vc.lastNameTextField.text = "Guy"
        vc.emailTextField.text = "thisIs@valid.com"
        vc.passwordTextField.text = "short"
        vc.confirmPasswordTextField.text = vc.passwordTextField.text!
        
        vc.registerButtonPressed()
        
        guard let alert = vc.presentedViewController as? UIAlertController else {
            XCTFail(); return
        }
        
        XCTAssertEqual(alert.title, error.title)
        XCTAssertEqual(alert.message, error.localizedDescription)
    }
    
    // MARK: - RegisterVC Asynchronouse Netwo
    func testAccountAlreadyExists() {
        let error = NetworkingError.errorJSON("Email has already been taken")
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.titleSegmentControl.selectedSegmentIndex = 0
        vc.firstNameTextField.text = "Some"
        vc.lastNameTextField.text = "Guy"
        vc.emailTextField.text = "alorro3@tcnj.edu"
        vc.passwordTextField.text = "password"
        vc.confirmPasswordTextField.text = vc.passwordTextField.text!
        
        let failExpectation = expectation(description: "Querying Registration Endpoint")
        
        vc.registerButtonPressed()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) { [unowned self] in
            guard let alert = self.vc.presentedViewController as? UIAlertController else {
                XCTFail(); return
            }
            
            XCTAssertEqual(error.title, alert.title)
            XCTAssertEqual(error.localizedDescription, alert.message)
            failExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
    }
    
    func testSuccessfulRegistration() {
        
        vc.roleSegmentControl.selectedSegmentIndex = 1
        vc.titleSegmentControl.selectedSegmentIndex = 0
        vc.firstNameTextField.text = "Some"
        vc.lastNameTextField.text = "Guy"
        vc.emailTextField.text = "someValid@email.com"
        vc.passwordTextField.text = "password"
        vc.confirmPasswordTextField.text = vc.passwordTextField.text!
        
        let successExpectation = expectation(description: "Querying Registration Endpoint")
        
        vc.registerButtonPressed()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) { [unowned self] in
            
            guard let alert = self.vc.presentedViewController as? UIAlertController else {
                XCTFail(); return
            }
            
            XCTAssertEqual(RegisterVC.AlertText.title.rawValue, alert.title)
            XCTAssertEqual(RegisterVC.AlertText.message.rawValue, alert.message)
            successExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
    }
    
    
    
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
