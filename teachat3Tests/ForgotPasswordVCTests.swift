//
//  ForgotPasswordVCTests.swift
//  teachat3
//
//  Created by Julio Alorro on 11/9/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import XCTest
@testable import teachat3

class ForgotPasswordVCTests: XCTestCase {
    
    fileprivate var vc: ForgotPasswordVC!
    
    override func setUp() {
        super.setUp()
        
        vc = ForgotPasswordVC()
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = vc
        window.makeKeyAndVisible()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - ForgotPasswordVC Textfield Conditionals
    
    func testInvalidEmail() {
        
        vc.emailTextField.text = "notAnEmail"
        
        // "Invalid Input: Email is not valid"
        let error = TeachatError.invalidInput(ForgotPasswordVC.ErrorText.invalidEmail.rawValue)
        
        let errorMessage = "\(error.title): \(error.localizedDescription)"
        
        vc.getPasswordButtonPressed()
        
        XCTAssertEqual(vc.errorLabel.text, errorMessage)
        
    }
    
    func testMissingEmail() {
        vc.emailTextField.text = ""
        
        let error = TeachatError.missingInput(ForgotPasswordVC.ErrorText.missing.rawValue)
        
        let errorMessage = "\(error.title): \(error.localizedDescription)"
        
        vc.getPasswordButtonPressed()
        
        XCTAssertEqual(vc.errorLabel.text, errorMessage)
    }
    
    // MARK: - ForgotPasswordVC Asynchronous Networking
    func testEmailDoesNotExist() {
        
        vc.emailTextField.text = "anEmailthat@DoesNotExist.com"
        
        let error = NetworkingError.errorJSON("Email not found.")
        
        let errorMessage = "\(error.title): \(error.localizedDescription)"
        
        let failExpectation = expectation(description: "Querying Password Reset Endpoint")
        
        vc.getPasswordButtonPressed()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            XCTAssertEqual(self.vc.errorLabel.text, errorMessage)
            failExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
    }
    
    func testSuccess() {
        vc.emailTextField.text = "alorro3@tcnj.edu"
        
        let successExpectation = expectation(description: "Querying Password Reset Endpoint")
        
        vc.getPasswordButtonPressed()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) { [unowned self] in
            
            // MARK: - Sanity Check assertion
            XCTAssertTrue(self.vc.presentedViewController is UIAlertController)
            
            guard let alert = self.vc.presentedViewController as? UIAlertController else {
                XCTFail(); return
            }

            XCTAssertEqual(alert.title, ForgotPasswordVC.AlertText.title.rawValue)
            XCTAssertEqual(alert.message, ForgotPasswordVC.AlertText.message.rawValue)
            successExpectation.fulfill()
            
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
