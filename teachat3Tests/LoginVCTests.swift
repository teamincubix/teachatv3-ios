//
//  teachat3Tests.swift
//  teachat3Tests
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import XCTest
@testable import teachat3

class LoginVCTests: XCTestCase {
    
    fileprivate var vc: LoginVC!
    fileprivate var appDelegate: AppDelegate = AppDelegate()
    
    override func setUp() {
        super.setUp()
        
        vc = LoginVC(
            apiDelegate: APIManager(),
            userDelegate: JSONManager(),
            loginDelegate: appDelegate,
            saveDelegate: DataManager()
        )
        
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = vc
        appDelegate.window?.makeKeyAndVisible()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        vc = nil
    }
    
    // MARK: - LoginVC Modal Presentations
    func testForgotPasswordPressed() {
        vc.forgotPasswordButtonPressed()
        XCTAssertTrue(vc.presentedViewController is ForgotPasswordVC)
    }
    
    func testRegisterButtonPressed() {
        vc.registerButtonPressed()
        XCTAssertTrue(vc.presentedViewController is RegisterVC)
    }
    
    // MARK: - LoginVC Textfield Conditionals
    func testEmptyTextFields() {
        
        vc.emailTextField.text = ""
        vc.passwordTextField.text = ""
        
        // "Missing Input: Missing Email/Password"
        let error = TeachatError.missingInput(LoginVC.ErrorText.missing.rawValue)
        
        let errorMessage = "\(error.title): \(error.localizedDescription)"
        
        vc.loginButtonPressed()
        
        XCTAssertEqual(vc.errorLabel.text, errorMessage)
    }
    
    func testInvalidEmail() {
        
        vc.emailTextField.text = "notAnEmail"
        vc.passwordTextField.text = "password"
        
        // "Invalid Input: Email is not valid"
        let error = TeachatError.invalidInput(LoginVC.ErrorText.invalidEmail.rawValue)
        
        let errorMessage = "\(error.title): \(error.localizedDescription)"
        
        vc.loginButtonPressed()
        
        XCTAssertEqual(vc.errorLabel.text, errorMessage)

    }
    
    // MARK: - LoginVC Asynchronous Networking
    
    //May fail due to slow internet connection, rerun multiple times if you have to
    func testWrongCredentials() {
        
        vc.emailTextField.text = "julio@hybols.com"
        vc.passwordTextField.text = "wrong password"
        
        let failExpectation = expectation(description: "Waiting for API Query")
        
        let error = NetworkingError.errorJSON("These credentials do not match our records.")
        
        let errorMessage = "\(error.title): \(error.localizedDescription)"
        
        vc.loginButtonPressed()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) { [unowned self] in
            XCTAssertEqual(self.vc.errorLabel.text, errorMessage)
            failExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
    }
    
    
    func testSuccessfulLogin() {
        vc.emailTextField.text = "julio@hybols.com"
        vc.passwordTextField.text = "password"
        
        let successExpectation = expectation(description: "Waiting for API Query")
        
        vc.loginButtonPressed()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) { [unowned self] in
            // MARK: - Sanity Check assertions
//            XCTAssertTrue(self.appDelegate.window?.rootViewController is UITabBarController)
//            XCTAssertTrue(rootVC.viewControllers?[0] is UINavigationController)
            
            guard let rootVC = self.appDelegate.window?.rootViewController as? UITabBarController else {
                XCTFail(); return
            }
            
            guard let navVC = rootVC.viewControllers?[0] as? UINavigationController else {
                XCTFail(); return
            }
            
            XCTAssertTrue(navVC.viewControllers[0] is AnnouncementVC)
            
            successExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.0, handler: nil)
        
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
