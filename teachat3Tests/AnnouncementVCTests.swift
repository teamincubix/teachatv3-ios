//
//  AnnouncementVCTests.swift
//  teachat3
//
//  Created by Julio Alorro on 11/15/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import XCTest
@testable import teachat3

class AnnouncementVCTests: XCTestCase {
    
    fileprivate var vc: AnnouncementVC!
    fileprivate var token: String!
    fileprivate let apiManager = APIManager()
    fileprivate var jsonManager = JSONManager()
    fileprivate var dataManager = DataManager()
    
    override func setUp() {
        super.setUp()
        vc = AnnouncementVC(apiDelegate: self.apiManager, dataDelegate: self.dataManager, announcementDelegate: self.jsonManager)
        
        let parameters = [
            JSONKey.email.rawValue : "julio@hybols.com",
            JSONKey.password.rawValue: "password"
        ]
        
        vc.apiDelegate.query(endpoint: APIManager.Endpoint.login, parameters: parameters) {
            [unowned self] (result: Result) in
            
            switch result {
                case .Success(let data):
                    let user = try! self.jsonManager.createUser(with: data)
                    
                    switch self.dataManager.save(data: user) {
                        
                        case true:
                            // Sanity Checks
                            print(self.vc.dataDelegate.getData())
                            print("Token: \(self.vc.token)")
                            break
                        case false:
                            fatalError("User did not save")
                    }
                
                case .Error(let error):
                    print(error)
            }
            
        }
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let navigationVC = UINavigationController(rootViewController: vc)
        let tabbarController = UITabBarController()
        tabbarController.viewControllers = [navigationVC]
        window.rootViewController = tabbarController
        window.makeKeyAndVisible()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /*
     CAVEAT:
     User data has to exist in the Keychain already for this test to succeed. If it doesn't you have to run the simulator and login normally (to populate keychain) then run this test after
    */
    func testGetAnnouncements() {
        vc.getAnnouncements()
        
        let successExpectation = expectation(description: "Querying Announcement Get endpoint")
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 10.0) { [unowned self] in
            
            XCTAssertFalse(self.vc.dataSource.objects.isEmpty)
            successExpectation.fulfill()
            
        }
        
        waitForExpectations(timeout: 11.0, handler: nil)
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
