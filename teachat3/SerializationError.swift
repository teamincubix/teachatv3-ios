//
//  SerializationError.swift
//  teachat3
//
//  Created by Julio Alorro on 11/7/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

enum SerializationError: DisplayableError {
    case jsonSerializaionFailed(String)
    case missingKey(String)
    case typecastFailed(String)
    
    var localizedDescription: String {
        switch self {
        case .jsonSerializaionFailed(let string):
            return string
            
        case .missingKey(let string):
            return string
            
        case .typecastFailed(let string):
            return string
        }
    }
    
    var title: String {
        switch self {
        case .jsonSerializaionFailed:
            return "JSON Serialization Failed"
            
        case .missingKey:
            return "Missing Key"
            
        case .typecastFailed:
            return "Typecasting Failed"
        }
    }
}
