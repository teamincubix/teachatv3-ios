//
//  Announcement.swift
//  teachat3
//
//  Created by Julio Alorro on 10/3/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Announcement {
    let id: Int
    let userID: Int
    let schoolID: Int
    let publishDate: Date
    let expireDate: Date
    let body: String
    let title: String
    let author: Author
    let schoolLogo: String
    let subjects: [Subject]?
}

struct Author {
    let title: String
    let firstName: String
    let lastName: String
    
    var fullName: String {
        if !title.isEmpty {
            return "\(firstName) \(lastName)"
        } else {
            return "\(title) \(firstName) \(lastName)"
        }
    }
}
