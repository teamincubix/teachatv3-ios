//
//  TableReloading.swift
//  teachat3
//
//  Created by Julio Alorro on 10/14/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol TableReloading: class {
    
    var reloadOnAppear: Bool { get set }
    
}
