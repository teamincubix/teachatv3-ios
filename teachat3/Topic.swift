//
//  Topic.swift
//  teachat3
//
//  Created by Julio Alorro on 10/11/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Topic {
    let id: Int
    let name: String
}

extension Topic: Equatable {
    public static func ==(lhs: Topic, rhs: Topic) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}
