//
//  Grade.swift
//  teachat3
//
//  Created by Julio Alorro on 10/12/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Grade {
    let id: Int
    let name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    init?(dictionary: [String: Any]) {
        guard let id = dictionary[JSONKey.id.rawValue] as? Int,
            let name = dictionary[JSONKey.description.rawValue] as? String
            else { print("Grade initialization failed"); return nil }
        
        self.id = id
        self.name = name
    }
}

extension Grade: Saveable {
    var dictionaryRepresentation: [String: Any] {
        return [
            JSONKey.id.rawValue: self.id,
            JSONKey.description.rawValue: self.name
        ]
    }
}

extension Grade: Equatable {
    public static func ==(lhs: Grade, rhs: Grade) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}
