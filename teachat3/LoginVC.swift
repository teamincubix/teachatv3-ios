//
//  ViewController.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit
import SwiftKeychainWrapper

final class LoginVC: UIViewController, InteractionHandling, AlertHandling, TargetActionSetting {
    
    // MARK: Custom Initializer
    init(apiDelegate: APIQuerying, userDelegate: UserCreating, loginDelegate: LoginDelegate, saveDelegate: DataSaving) {
        self.apiDelegate = apiDelegate
        self.userDelegate = userDelegate
        self.loginDelegate = loginDelegate
        self.saveDelegate = saveDelegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Delegate Properties
    fileprivate let apiDelegate: APIQuerying
    fileprivate var userDelegate: UserCreating
    fileprivate let loginDelegate: LoginDelegate
    fileprivate let saveDelegate: DataSaving
    
    override func loadView() {
        view = LoginView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTextfieldDelegates()
        self.registerForKeyboardNotifications()
        self.setUpTargetActions([
            self.loginButton: #selector(self.loginButtonPressed),
            self.facebookButton: #selector(self.facebookButtonPressed),
            self.forgotPasswordButton: #selector(self.forgotPasswordButtonPressed),
            self.registerButton: #selector(self.registerButtonPressed)
        ])
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let textField = self.activeTextField {
            textField.resignFirstResponder()
        }
        
        super.touchesBegan(touches, with: event)
    }
    
    deinit {
        print("\(self) was deallocated")
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate weak var activeTextField: UITextField?
    fileprivate let keyboardHeightDivisor: CGFloat = 4.0
    
    fileprivate lazy var alert: UIAlertController = {
        return UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    }()
    
    fileprivate lazy var textFields: [TeachatTextField] = [
        self.emailTextField, self.passwordTextField
    ]
    
    fileprivate lazy var buttons: [UIButton] = [
            self.loginButton, self.facebookButton, self.forgotPasswordButton, self.registerButton
    ]
    
    let endpoint: APIManager.Endpoint = .login
    
}

// MARK: - Computed Properties (and IBOutlets)
extension LoginVC {
    fileprivate weak var loginView: LoginView! { return self.view as! LoginView }
    weak var emailTextField: TeachatTextField! { return self.loginView.emailTextField }
    weak var passwordTextField: TeachatTextField! { return self.loginView.passwordTextField }
    fileprivate weak var loginButton: TeachatButton! { return self.loginView.loginButton }
    fileprivate weak var facebookButton: UIButton! { return self.loginView.facebookButton }
    fileprivate weak var forgotPasswordButton: TeachatButton! { return self.loginView.forgotPasswordButton }
    fileprivate weak var registerButton: TeachatButton! { return self.loginView.registerButton }
    fileprivate weak var activityIndicator: UIActivityIndicatorView! { return self.loginView.activityIndicator }
    weak var errorLabel: TeachatLabel! { return self.loginView.errorLabel }
    fileprivate weak var loginStackView: UIStackView! { return self.loginView.loginStackView }
    
    fileprivate weak var logo: UIImageView! { return self.loginView.logo }
    fileprivate weak var bottomLogoConstraint: Constraint! { return self.loginView.bottomLogoConstraint }
    
}

// MARK: - Setup Functions for Textfield delegates, NotificationCenter
fileprivate extension LoginVC {
    
    func setUpTextfieldDelegates() {
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
    }
    
    @objc func keyboardWasShown(_ notification: Notification) {
        self.changeUserInteraction(of: self.buttons, to: false)
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0.0 {
                UIView.animate(withDuration: 0.25) { [unowned self] in
                    self.view.frame.origin.y -= keyboardSize.height / self.keyboardHeightDivisor
                }
            }
        }
    }
    
    @objc func keyboardWasDismissed(_ notification: Notification) {
        self.changeUserInteraction(of: self.buttons, to: true)
        self.view.frame.origin.y = 0.0
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasDismissed(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    func animate(shakeables: [Shakeable], error: DisplayableError) {
        self.errorLabel.text = "\(error.title): \(error.localizedDescription)"
        
        self.errorLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(self.loginView).multipliedBy(TeachatUI.View.ratio * 1.1)
            
        }
        self.errorLabel.superview?.layoutIfNeeded()
        
        self.errorLabel.fade(rootView: self.view)
        
        _ = shakeables.map{ $0.shake() }
        
        
    }
}

// MARK: - Textfield Delegate Function
extension LoginVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.activeTextField = textField
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.activeTextField = nil
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let textField = textField as? TeachatTextField
            else { print("Could not typecast textfield"); return false }
        
        guard let lastTextField = self.textFields.last
            else { print("There are no elements in textFields array"); return false }
        
        if textField != lastTextField {
            
            guard let index = self.textFields.index(of: textField)
                else { print("textField is not in array"); return false }
            
            self.textFields[index + 1].becomeFirstResponder()
            
        } else if textField == lastTextField {
            
            textField.resignFirstResponder()
            
            if let email = emailTextField.text, !email.isEmpty, let password = passwordTextField.text, !password.isEmpty {
                self.loginButtonPressed()
            }
        }
        
        return false

    }
}

// MARK: - Target Action Functions (IBActions)
extension LoginVC {
    
    enum ErrorText: String {
        case invalidEmail = "Email is not valid"
        case missing = "Missing Email/Password"
        case keychain = "Could not save credentials. Please report error"
    }
    
    @objc func loginButtonPressed() {
        
        self.loginButton.spring()
        
        if let email = emailTextField.text , !email.isEmpty, let password = passwordTextField.text , !password.isEmpty {
            
            guard email.isValidEmail else {
                
                self.animate(shakeables: self.textFields, error: TeachatError.invalidInput(ErrorText.invalidEmail.rawValue)); return
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.activityIndicator.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            
            let parameters: [String: Any] = [
                JSONKey.email.rawValue: email,
                JSONKey.password.rawValue: password
            ]
            
            self.apiDelegate.query(endpoint: self.endpoint, parameters: parameters) { [unowned self]
                (result: Result) in
                
                DispatchQueue.main.async {
                    
                    switch result {
                        case .Success(let data):
                            do {
                                let user = try self.userDelegate.createUser(with: data)
                                switch self.saveDelegate.save(data: user) {
                                    case true:
                                        print("Saved")
                                        self.loginDelegate.changeRootViewToTabBarVC()
                                    
                                    case false:
                                        let error = TeachatError.appSide(ErrorText.keychain.rawValue)
                                        self.animate(shakeables: self.textFields, error: error)
                                
                                }
                            } catch let error {
                                
                                guard let error = error as? DisplayableError
                                    else { print("Error type not handled at \(self) line 246"); return }
                                
                                // TODO: - Consider using "Unexpected Error: Something Unexpected occurred please report to our email"
                                
                                self.animate(shakeables: self.textFields, error: error)
                                
                            }
                        case .Error(let error):
                        
                            guard let error = error as? DisplayableError else { print("Error at \(self) line 255"); return }
                            self.animate(shakeables: self.textFields, error: error)
                    }
                    
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.activityIndicator.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                
                }
            }
            
        } else {

            self.animate(shakeables: self.textFields, error: TeachatError.missingInput(ErrorText.missing.rawValue))

        }
    }
    
    @objc func forgotPasswordButtonPressed() {
        forgotPasswordButton.spring()
        
        let forgotPasswordVC = ForgotPasswordVC()
        self.present(forgotPasswordVC, animated: true, completion: nil)
        
    }
    
    @objc func registerButtonPressed() {
        registerButton.spring()
        
        let registerVC = RegisterVC()
        self.present(registerVC, animated: true, completion: nil)
    }
    
    @objc func facebookButtonPressed() {
        print("Facebook!")
    }
}
