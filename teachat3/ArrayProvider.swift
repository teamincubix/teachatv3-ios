//
//  ArrayProvider.swift
//  teachat3
//
//  Created by Julio Alorro on 11/14/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct ArrayProvider<Type>: DataProviding {
    var objects: [Type]
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(inSection: Int) -> Int {
        return objects.count
    }
    
    func object(at indexPath: IndexPath) -> Type {
        return objects[indexPath.row]
    }
}
