//
//  DataGetting.swift
//  teachat3
//
//  Created by Julio Alorro on 9/30/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol DataGetting {
    
    var hasValue: Bool { get }
    
    func getData() -> [String: Any]?
    
}
