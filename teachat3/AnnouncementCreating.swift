//
//  AnnouncementCreating.swift
//  teachat3
//
//  Created by Julio Alorro on 10/3/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol AnnouncementCreating {
    
    mutating func createAnnouncements(with data: Data) throws -> [Announcement]

}

