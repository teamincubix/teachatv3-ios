//
//  AnnouncementEditing.swift
//  teachat3
//
//  Created by Julio Alorro on 10/24/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol AnnouncementEditing: class {
    func editAnnouncement(at indexPath: IndexPath)
}
