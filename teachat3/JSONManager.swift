//
//  JSONManager.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct JSONManager {
    
    fileprivate enum ErrorText: String {
        case typecast = "Typecasting failed"
        case data = "Data could not be serialized"
    }
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
}

fileprivate extension JSONManager {
    func createJSON(with data: Data) throws -> [String: Any] {
        
        do {
            
            let json = try JSONSerialization.jsonObject(with: data)
            
            guard let dict = json as? [String: Any]
                else { throw SerializationError.jsonSerializaionFailed("\(ErrorText.typecast.rawValue) of JSON to [String: Any]") }
            
            return dict
            
        } catch {
            
            throw SerializationError.jsonSerializaionFailed(ErrorText.data.rawValue)
            
        }
        
    }
    
//    func dict(of dict: [String: Any], forKey key: String) throws -> [String: Any] {
//        guard let dictNode = dict[key] as? [String: Any]
//            else { throw SerializationError.typecastFailed(ErrorText.typecast.rawValue + " to dictionary with \(key) key") }
//        
//        return dictNode
//    }
    
    func dict(of dict: [String: Any], forKey key: JSONKey) throws -> [String: Any] {
        
        guard let dictNode = dict[key.rawValue] as? [String: Any]
            else { throw SerializationError.typecastFailed(ErrorText.typecast.rawValue + " to dictionary with \(key) key") }
        
        return dictNode
        
    }
    
    func array(of dict: [String: Any] , forKey key: JSONKey) throws -> [[String: Any]] {
        guard let arrayNode = dict[key.rawValue] as? [[String: Any]]
            else { throw SerializationError.typecastFailed(ErrorText.typecast.rawValue + " to array with \(key) key") }
        
        return arrayNode
    }
    
    func string(of dict: [String: Any], forKey key: JSONKey) throws -> String {
        guard let string = dict[key.rawValue] as? String
            else { throw SerializationError.typecastFailed(ErrorText.typecast.rawValue + " to string with \(key) key") }
        
        return string
    }
    
    func int(of dict: [String: Any], forKey key: JSONKey) throws -> Int {
        guard let int = dict[key.rawValue] as? Int
            else { throw SerializationError.typecastFailed(ErrorText.typecast.rawValue + " to int with \(key) key") }
        
        return int
    }
    
    func createSubject(from subjectNode: [String: Any], subjectID: Int) throws -> Subject {
        
        let subjectName = try string(of: subjectNode, forKey: .subject)
        
        let gradeNode = try dict(of: subjectNode, forKey: .grade)
        let gradeID = try int(of: gradeNode, forKey: .id)
        let gradeName = try string(of: gradeNode, forKey: .description)
        
        let topicNode = try dict(of: subjectNode, forKey: .topic)
        let topicID = try int(of: topicNode, forKey: .id)
        let topicName = try string(of: topicNode, forKey: .description)
        
        let topic = Topic(
            id: topicID,
            name: topicName
        )
        
        let grade = Grade(
            id: gradeID,
            name: gradeName
        )
        
        let subject = Subject(
            id: subjectID,
            name: subjectName,
            grade: grade,
            topic: topic
        )
        
        return subject
        
    }
    
}

extension JSONManager: UserCreating {
    
    mutating func createUser(with data: Data) throws -> User {
        
        let json = try createJSON(with: data)
        
        let dataNode = try dict(of: json, forKey: .data)
        
        let token = try string(of: dataNode, forKey: .token)
        
        let userNode = try dict(of: dataNode, forKey: .user)
        
        let id = try int(of: userNode, forKey: .id)
        
        let roleID = try int(of: userNode, forKey: .roleID)
        
        // Key to parse child data
        var childKey: JSONKey
        
        switch roleID {
        case 2:
            childKey = .studentDetails
            
        case 3:
            childKey = .children
            
        default:
            throw TeachatError.invalidRole
        }
        
        let email = try string(of: userNode, forKey: .email)
        
        let firstName = try string(of: userNode, forKey: .firstName)
        let middleName = userNode[JSONKey.middleName.rawValue] as? String
        let lastName = try string(of: userNode, forKey: .lastName)
        
        let gender = userNode[JSONKey.gender.rawValue] as? String
        let birthDateString = userNode[JSONKey.birthdate.rawValue] as? String
        
        var birthDate: Date?
        
        if let birthDateString = birthDateString {
            birthDate = dateFormatter.date(from: birthDateString)
        }
        
        let addressOne = userNode[JSONKey.addressOne.rawValue] as? String
        let addressTwo = userNode[JSONKey.addressTwo.rawValue] as? String
        let city = userNode[JSONKey.city.rawValue] as? String
        //        let state = try convertJSONNodeToString(json["user"]?["state"]??["state_name"])
        //        let country = try convertJSONNodeToString(json["user"]?["state"]??["country"])
        
        let cell = userNode[JSONKey.cell.rawValue] as? String
        let home = userNode[JSONKey.home.rawValue] as? String
        let work = userNode[JSONKey.work.rawValue] as? String
        
        var children: [Child] = []
        
        if let childrenData = userNode[childKey.rawValue] as? [[String: Any]] {
            
            for childData in childrenData {
                let childNode = try dict(of: childData, forKey: .child)
                let id = try int(of: childNode, forKey: .id)
                let firstName = try string(of: childNode, forKey: .firstName)
                let middleName = childNode[JSONKey.middleName.rawValue] as? String
                let lastName = try string(of: childNode, forKey: .lastName)
                
                let birthDateString = try string(of: childNode, forKey: .birthdate)
                
                guard let birthDate = dateFormatter.date(from: birthDateString) else {
                    let errorString = ErrorText.typecast.rawValue + " with \(JSONKey.birthdate.rawValue) key"
                    throw SerializationError.typecastFailed(errorString)
                }
                
                let gender = try string(of: childNode, forKey: .gender)
                let city = try string(of: childNode, forKey: .city)
                let section = try string(of: childNode, forKey: .section)
                
                let gradeNode = try dict(of: childNode, forKey: .grade)
                let gradeID = try int(of: gradeNode, forKey: .id)
                let gradeName = try string(of: gradeNode, forKey: .description)
                
                let schoolNode = try dict(of: childNode, forKey: .school)
                let schoolID = try int(of: schoolNode, forKey: .id)
                let schoolName = try string(of: schoolNode, forKey: .schoolName)
                let schoolLogo = try string(of: schoolNode, forKey: .schoolLogo)
                
                let stateNode = try dict(of: schoolNode, forKey: .state)
                let stateID = try int(of: stateNode, forKey: .id)
                let stateName = try string(of: stateNode, forKey: .stateName)
                
                let countryNode = try dict(of: stateNode, forKey: .country)
                let countryID = try int(of: countryNode, forKey: .id)
                let countryName = try string(of: countryNode, forKey: .name)
                
                let grade = Grade(id: gradeID, name: gradeName)
                
                let country = Country(name: countryName, id: countryID)
                
                let state = State(name: stateName, id: stateID, country: country)
                
                let school = School(name: schoolName, id: schoolID, logo: schoolLogo, state: state)
                
                let child = Child(
                    id: id,
                    firstName: firstName,
                    middleName: middleName,
                    lastName: lastName,
                    birthDate: birthDate,
                    gender: gender,
                    city: city,
                    section: section,
                    grade: grade,
                    school: school
                )
                
                children.append(child)
                
            }
        } else {
            
            print("No children array")
            
        }
        
        let user = User(
            
            token: token,
            id: id,
            roleID: roleID,
            email: email,
            firstName: firstName,
            middleName: middleName,
            lastName: lastName,
            gender: gender,
            birthDate: birthDate,
            addressOne: addressOne,
            addressTwo: addressTwo,
            city: city,
            //            state: state,
            //            country: country,
            cell: cell,
            home: home,
            work: work,
            children: children
            
        )
        
        return user
        
    }
}

extension JSONManager: LocationCreating {
    func createLocation(with data: Data) throws -> Location {
        let json = try createJSON(with: data)
        
        let dataNode = try dict(of: json, forKey: .data)
        
        let schoolsNode = try array(of: dataNode, forKey: .schools)
        
        var countryArray: [Country] = []
        var stateDict: [Country: [State]] = [:]
        var stateArray: [State] = []
        var schoolArray: [School] = []
        var schoolDict: [State: [School]] = [:]
        
        for schoolNode in schoolsNode {
            let schoolName = try string(of: schoolNode, forKey: .schoolName)
            let schoolID = try int(of: schoolNode, forKey: .id)
            let schoolLogo = try string(of: schoolNode, forKey: .schoolLogo)
            
            let stateNode = try dict(of: schoolNode, forKey: .state)
            let stateName = try string(of: stateNode, forKey: .stateName)
            let stateID = try int(of: stateNode, forKey: .id)
            
            let countryNode = try dict(of: stateNode, forKey: .country)
            let countryName = try string(of: countryNode, forKey: .name)
            let countryID = try int(of: countryNode, forKey: .id)
            
            let country: Country = Country(
                name: countryName,
                id: countryID
            )
            
            let state: State = State(
                name: stateName,
                id: stateID,
                country: country
            )
            
            let school: School = School(
                name: schoolName,
                id: schoolID,
                logo: schoolLogo,
                state: state
            )
            
            schoolArray.append(school)
            stateArray.append(school.state)
            countryArray.append(school.state.country)
        }
        
        // Remove Duplicates
        stateArray = Array(Set(stateArray))
        stateArray.sort { $0.id < $1.id }
        
        countryArray = Array(Set(countryArray))
        countryArray.sort { $0.id < $1.id }
        
        
        // MARK: - O(n^2) State Dict
        for country in countryArray {
            var states: [State] = []
            
            for state in stateArray {
                
                if state.country == country {
                    states.append(state)
                }
            }
            stateDict[country] = states
        }
        
        // MARK: - O(n^2) School Dict
        for state in stateArray {
            var schools: [School] = []
            
            for school in schoolArray {
                if school.state == state {
                    schools.append(school)
                }
            }
            
            schoolDict[state] = schools
        }
        
        let location = Location(
            countries: countryArray,
            states: stateDict,
            schools: schoolDict
        )
        
        return location
    }
}

extension JSONManager: AnnouncementCreating {
    mutating func createAnnouncements(with data: Data) throws -> [Announcement] {
        let json = try createJSON(with: data)
        
        let dataNode = try array(of: json, forKey: .data)
        
        var announcements: [Announcement] = []
        
        for announcementNode in dataNode {
            let id = try int(of: announcementNode, forKey: .id)
            let userID = try int(of: announcementNode, forKey: .userID)
            let schoolID = try int(of: announcementNode, forKey: .schoolID)
            
            let publishDateString = try string(of: announcementNode, forKey: .publishDate)
            
            guard let publishDate = dateFormatter.date(from: publishDateString) else {
                let error = ErrorText.typecast.rawValue + " with \(JSONKey.publishDate.rawValue) key"
                throw SerializationError.typecastFailed(error)
            }
            
            let expireDateString = try string(of: announcementNode, forKey: .expireDate)
            
            guard let expireDate = dateFormatter.date(from: expireDateString) else {
                let error = ErrorText.typecast.rawValue + " with \(JSONKey.expireDate.rawValue) key"
                throw SerializationError.typecastFailed(error)
            }
            
            let body = try string(of: announcementNode, forKey: .announcement)
            let title =  try string(of: announcementNode, forKey: .title)
            
            let authorNode = try dict(of: announcementNode, forKey: .postedBy)
            let authorTitle = try string(of: authorNode, forKey: .title)
            let firstName = try string(of: authorNode, forKey: .firstName)
            let lastName = try string(of: authorNode, forKey: .lastName)
            
            let author = Author(title: authorTitle, firstName: firstName, lastName: lastName)
            
            let schoolNode = try dict(of: announcementNode, forKey: .school)
            let schoolLogo = try string(of: schoolNode, forKey: .schoolLogo)
            
//            let subjectsArray = try array(of: announcementNode, forKey: .announcementSubjects)
            let subjectsArray = announcementNode[JSONKey.announcementSubjects.rawValue] as? [[String: Any]]
            
            var subjects: [Subject] = []
            
            if let subjectsArray = subjectsArray, !subjectsArray.isEmpty {
                
                for subjectElement in subjectsArray {
                    let subjectID = try int(of: subjectElement, forKey: .teacherSubjectID)
                    let teacherSubjectNode = try dict(of: subjectElement, forKey: .teacherSubject)
                    
                    let subjectNode = try dict(of: teacherSubjectNode, forKey: .subject)
                    
                    let subject = try createSubject(from: subjectNode, subjectID: subjectID)
                    subjects.append(subject)
                }
                
            }
            
            let announcement = Announcement(
                id: id,
                userID: userID,
                schoolID: schoolID,
                publishDate: publishDate,
                expireDate: expireDate,
                body: body,
                title: title,
                author: author,
                schoolLogo: schoolLogo,
                subjects: subjects
            )
            
            announcements.append(announcement)
        }
        
        return announcements
    }
}

extension JSONManager: SubjectCreating {
    func createSubjects(with data: Data) throws -> [Subject] {
        
        let json = try createJSON(with: data)
        
        let dataNode = try array(of: json, forKey: .data)
        
        var subjects: [Subject] = []
        
        for subjectNode in dataNode {
            
            let subjectID = try int(of: subjectNode, forKey: .id)
            
            // Inner subject node
            let subjectNode = try dict(of: subjectNode, forKey: .subject)
            
            let subject = try createSubject(from: subjectNode, subjectID: subjectID)
            
            subjects.append(subject)
        }
        return subjects
    }
}
