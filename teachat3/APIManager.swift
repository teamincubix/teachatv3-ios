//
//  APIManager.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.


import Foundation

protocol APIEndpoint {
    
    associatedtype Endpoint
    
}

struct APIManager: APIEndpoint {
    
    fileprivate let session = URLSession(configuration: URLSessionConfiguration.default)
    
    enum Endpoint: String {
        // API Endpoints
        fileprivate static let scheme = "https"
        fileprivate static let host = "api.teachat.co"
        fileprivate static let path = "/v1/"
        
        case login = "user/auth"
        case passwordReset = "user/reset"
        case location = "school_country/get"
        case registration = "user/register"
        case announcementPost = "announcement/post"
        case announcementGet = "announcement/get"
        case announcementUpdate = "announcement/update"
        case announcementDelete = "announcement/delete"
        case subjectGet = "subject/get"
        
        fileprivate var urlComponents: URLComponents {
            var urlComponents = URLComponents()
            urlComponents.scheme = Endpoint.scheme
            urlComponents.host = Endpoint.host
            urlComponents.path = Endpoint.path + rawValue
            return urlComponents
        }
    }
    
    fileprivate enum S3Directory {
        fileprivate static let scheme = "https"
        fileprivate static let host = "s3-ap-southeast-1.amazonaws.com"
        fileprivate static let path = "/teachatco/images/school_badges/"
        
        fileprivate static var urlComponents: URLComponents {
            var urlComponents = URLComponents()
            urlComponents.scheme = S3Directory.scheme
            urlComponents.host = S3Directory.host
            urlComponents.path = S3Directory.path
            return urlComponents
        }
        
        fileprivate static func appendToURL(with string: String) -> URL {
            var urlComponents = self.urlComponents
            urlComponents.path.append(string)
            return urlComponents.url!
        }
    }
    
    fileprivate enum HTTPMethod: String {
        case POST, GET, PUT, DELETE
    }
    
    fileprivate enum ErrorText: String {
        case httpBody = "JSON Serialization of http body failed"
    }
    
    fileprivate enum MissingParameter: String {
        case token
        case id
    }
    
    fileprivate let contentType: String = "Content-Type"
    fileprivate let applicationJSON: String = "application/json"
    
    fileprivate func createRequest(with endpoint: Endpoint, parameters: [String: Any]? = nil) throws -> URLRequest {
        var urlComponents = endpoint.urlComponents
        var httpBody: Data?
        var httpMethod: HTTPMethod
        var json: [String: Any] = [:]
        
        if let parameters = parameters {
            json = parameters
        }
        
        // Assign the appropriate httpMethod
        switch endpoint {
            
        case .login, .passwordReset, .registration, .announcementPost:
            httpMethod = HTTPMethod.POST
            
        case .location, .announcementGet, .subjectGet:
            httpMethod = HTTPMethod.GET
            
        case .announcementDelete:
            httpMethod = HTTPMethod.DELETE
            
        case .announcementUpdate:
            httpMethod = HTTPMethod.PUT
            
        }
        
        // Add URL parameters based on the endpoint
        switch endpoint {
            
        case .login, .passwordReset, .location, .registration:
            break
            
        case .announcementGet, .subjectGet:
            
            guard let json = json as? [String: String] else {
                throw NetworkingError.missingParameters(endpoint, MissingParameter.token.rawValue)
            }
            
            let queryListItem = json.map { URLQueryItem(name: $0, value: $1) }
            urlComponents.queryItems = queryListItem
            
        case .announcementDelete, .announcementUpdate:
            
            guard let id = json.removeValue(forKey: JSONKey.id.rawValue) as? String,
                let token = json.removeValue(forKey: JSONKey.token.rawValue) as? String
                else {
                    let errorString = "\(MissingParameter.id.rawValue) & \(MissingParameter.token.rawValue)"
                    throw NetworkingError.missingParameters(endpoint, errorString)
            }
            
            urlComponents.queryItems = [
                URLQueryItem(name: JSONKey.id.rawValue, value: id),
                URLQueryItem(name: JSONKey.token.rawValue, value: token)
            ]
        case .announcementPost:
            guard let token = json.removeValue(forKey: JSONKey.token.rawValue) as? String else {
                let errorString = "\(MissingParameter.token.rawValue) not removed"
                throw NetworkingError.missingParameters(endpoint, errorString)
            }
            
            urlComponents.queryItems = [
                URLQueryItem(name: JSONKey.token.rawValue, value: token)
            ]
        }
        
        switch httpMethod {
        case .POST, .PUT:
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json as Any)
                
                httpBody = jsonData
                
            } catch {
                
                throw SerializationError.jsonSerializaionFailed(ErrorText.httpBody.rawValue)
                
            }
            
        case .GET, .DELETE:
            break
        }
        
        guard let url = urlComponents.url else { throw NetworkingError.invalidURL(urlComponents) }
        
        var request = URLRequest(url: url)
        
        if let httpBody = httpBody {
            
            request.httpBody = httpBody
            
        }
        
        if request.value(forHTTPHeaderField: contentType) == nil {
            
            request.setValue(applicationJSON, forHTTPHeaderField: contentType)
            
        }
        
        request.httpMethod = httpMethod.rawValue
        
        return request
        
    }
}

extension APIManager: APIQuerying {
    
    func query(endpoint: Endpoint, parameters: [String : Any]?, completionHandler: @escaping (Result<Data>) -> Void) {
        
        do {
            
            let request = try createRequest(with: endpoint, parameters: parameters)
            let task = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                
                if let error = error as? NSError {
                    
                    completionHandler(Result.Error(NetworkingError.noInternet(error.localizedDescription)))
                    
                } else if let data = data, let response = response as? HTTPURLResponse {
                    
                    if response.statusCode >= 400 {
                        
                        print("Fail Response: \(response)")
                        
                        // Forced unwrapped these two lines to ensure the API is error free
                        let errorJSON = try! JSONSerialization.jsonObject(with: data) as! [String: Any]
                        print(errorJSON)
                        let errorString = errorJSON[JSONKey.result.rawValue] as! String
                        
                        completionHandler(Result.Error(NetworkingError.errorJSON(errorString)))
                        
                    } else if response.statusCode == 200 {
                        print("Response: \(response)")
                        completionHandler(Result.Success(data))
                        
                    }  else {
                        
                        fatalError("Unhandled HTTP Response")
                        
                    }
                }
            }
            
            task.resume()
            
            
        } catch let error {
            
            completionHandler(Result.Error(error))
            
        }
    }
}

extension APIManager: ImageFetching {
    func fetchImage(with string: String, completionHandler: @escaping (Result<Data>) -> Void) {
        let url = S3Directory.appendToURL(with: string)
        
        let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error as? NSError {
                
                completionHandler(Result.Error(NetworkingError.noInternet(error.localizedDescription)))
                
            } else if let data = data, let response = response as? HTTPURLResponse {
                
                if response.statusCode >= 400 {
                    
                    print("Fail Response: \(response)")
                    
                } else if response.statusCode == 200 {
                    
                    print(response)
                    completionHandler(Result.Success(data))
                    
                } else {
                    
                    fatalError("Unhandled HTTP Response")
                    
                }
            }
        }
        
        task.resume()
    }
}
