//
//  Saveable.swift
//  teachat3
//
//  Created by Julio Alorro on 10/10/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol Saveable {
    
    var dictionaryRepresentation: [String: Any] { get }
    
}
