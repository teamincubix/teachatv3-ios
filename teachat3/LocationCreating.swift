//
//  LocationDataDelegate.swift
//  teachat3
//
//  Created by Julio Alorro on 9/23/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol LocationCreating{
    
    func createLocation(with data: Data) throws -> Location

}
