//
//  Country.swift
//  teachat3
//
//  Created by Julio Alorro on 10/10/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Country {
    let name: String
    let id: Int
    
    init(name: String, id: Int) {
        self.name = name
        self.id = id
    }
    
    init?(dictionary: [String: Any]) {
        
        guard let name = dictionary[JSONKey.name.rawValue] as? String,
            let id = dictionary[JSONKey.id.rawValue] as? Int
            else { print("Country initialization failed"); return nil }
        
        self.name = name
        self.id = id
        
    }
}

extension Country: Hashable {
    var hashValue: Int {
        return name.hashValue ^ id.hashValue
    }
    
    public static func ==(lhs: Country, rhs: Country) -> Bool {
        return lhs.name == rhs.name && lhs.id == rhs.id
    }
}

extension Country: Saveable {
    var dictionaryRepresentation: [String: Any] {
        
        return [
            JSONKey.name.rawValue: self.name,
            JSONKey.id.rawValue: self.id
        ]
        
    }
}
