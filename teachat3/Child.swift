//
//  Child.swift
//  teachat3
//
//  Created by Julio Alorro on 10/4/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Child {
    let id: Int
    let firstName: String
    let middleName: String?
    let lastName: String
    let birthDate: Date
    let gender: String
    let city: String
    let section: String
    let grade: Grade
    let school: School
    
    init(id: Int, firstName: String, middleName: String?, lastName: String, birthDate: Date, gender: String, city: String, section: String, grade: Grade, school: School) {
        
        // Non Optional
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.birthDate = birthDate
        self.gender = gender
        self.city = city
        self.section = section
        self.grade = grade
        self.school = school
        
        // Optional
        self.middleName = middleName
    }
    
    init?(dictionary: [String: Any]) {
        
        guard let id = dictionary[JSONKey.id.rawValue] as? Int,
            let firstName = dictionary[JSONKey.firstName.rawValue] as? String,
            let lastName = dictionary[JSONKey.lastName.rawValue] as? String,
            let birthDate = dictionary[JSONKey.birthdate.rawValue] as? Date,
            let gender = dictionary[JSONKey.gender.rawValue] as? String,
            let city = dictionary[JSONKey.city.rawValue] as? String,
            let section = dictionary[JSONKey.section.rawValue] as? String,
            let gradeDict = dictionary[JSONKey.grade.rawValue] as? [String: Any],
            let grade = Grade(dictionary: gradeDict),
            let schoolDict = dictionary[JSONKey.school.rawValue] as? [String: Any],
            let school = School(dictionary: schoolDict)
            else { print("Child initialization failed"); return nil }
        
        let middleName = dictionary[JSONKey.middleName.rawValue] as? String
        
        self.id = id
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName
        self.birthDate = birthDate
        self.gender = gender
        self.city = city
        self.section = section
        self.grade = grade
        self.school = school
    }
}

extension Child: Saveable {
    var dictionaryRepresentation: [String: Any] {
        
        var dictionary: [String: Any] = [:]
        
        if let middleName = self.middleName {
            dictionary[JSONKey.middleName.rawValue] = middleName
        }
        
        dictionary[JSONKey.id.rawValue] = self.id
        dictionary[JSONKey.firstName.rawValue] = self.firstName
        dictionary[JSONKey.lastName.rawValue] = self.lastName
        dictionary[JSONKey.birthdate.rawValue] = self.birthDate
        dictionary[JSONKey.gender.rawValue] = self.gender
        dictionary[JSONKey.city.rawValue] = self.city
        dictionary[JSONKey.section.rawValue] = self.section
        dictionary[JSONKey.grade.rawValue] =  self.grade.dictionaryRepresentation
        dictionary[JSONKey.school.rawValue] = self.school.dictionaryRepresentation
        
        return dictionary
        
    }
}
