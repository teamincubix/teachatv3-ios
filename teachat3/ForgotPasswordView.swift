//
//  ForgotPasswordView.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class ForgotPasswordView: UIView {
    private enum Text: String {
        case email = "Please enter your email"
        case submit = "Get Password"
        case back = "Go Back"
    }
    
    fileprivate let background: UIImageView = {
        let imageView = UIImageView()
        imageView.image = TeachatUI.Image.background
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        return imageView
    }()

    fileprivate let logo: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.image = TeachatUI.Image.forgotPassword
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        return imageView
    }()
    
    let errorLabel: TeachatLabel = {
        let label = TeachatLabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 13.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.red
        label.backgroundColor = TeachatUI.Color.warningRed
        label.textAlignment = NSTextAlignment.center
        label.alpha = 0.0
        label.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return label
    }()
    
    fileprivate lazy var stackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.emailTextField, self.getPasswordButton, self.goBackButton])
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 10.0
        return stackView
    }()
    
    let emailTextField: TeachatTextField  = {
        let textField = TeachatTextField()
        textField.placeholder = Text.email.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.setUpKeyboard(UITextField.KeyboardType.email, returnKeyType: UIReturnKeyType.send)
        
        let imageView = UIImageView(image: TeachatUI.Image.password)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let getPasswordButton: TeachatButton = {
        let button = TeachatButton(type: UIButtonType.custom)
        button.setTitle(Text.submit.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.green
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()
    
    let goBackButton: TeachatButton = {
        let button = TeachatButton(type: UIButtonType.custom)
        button.setTitle(Text.back.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.red
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.backgroundColor = TeachatUI.Color.lightGray
        indicator.color = TeachatUI.Color.green
        indicator.hidesWhenStopped = true
        indicator.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return indicator
    }()
    
    private lazy var subviewsArray: [UIView] = [self.background, self.logo, self.errorLabel, self.stackView, self.activityIndicator]
    
    var centerLogoConstraint: Constraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setSubviewsForAutoLayout(subviewsArray)
        
        background.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.edges.equalTo(self)
        }
        
        stackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(self).multipliedBy(0.25)
            make.center.equalTo(self)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
        }
        
        errorLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio * 0.8)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.bottom.equalTo(self.stackView.snp.top).offset(-10.0)
        }
        
        logo.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            self.centerLogoConstraint = make.centerX.equalTo(self).constraint
            make.height.equalTo(self).multipliedBy(0.10)
            make.bottom.equalTo(self.errorLabel.snp.top).offset(-20.0)
            make.width.equalTo(self.logo.snp.height).multipliedBy(5.0)
        }
        
        activityIndicator.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.width.equalTo(self.activityIndicator.snp.height)
            make.height.equalTo(self).multipliedBy(0.10)
            make.center.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let leftView = self.emailTextField.leftView else { return }
        
        leftView.frame.size = CGSize(width: self.emailTextField.iconWidth, height: self.emailTextField.iconHeight)
        
    }
}
