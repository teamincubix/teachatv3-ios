//
//  AppDelegate.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    fileprivate var loginVC: LoginVC!
    fileprivate var tabBarVC: UITabBarController!
    fileprivate var dataDelegate: DataGetting!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        
        dataDelegate = DataManager()
        
        switch dataDelegate.hasValue {
            case true:
                setUpTabBarVC()
                window?.rootViewController = tabBarVC
            
            case false:
                setUpLoginVC()
                window?.rootViewController = loginVC
        }
    
        window?.backgroundColor = TeachatUI.Color.lightGray
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

// MARK: - Helper Functions
private extension AppDelegate {
    
    func setUpTabBarVC() {
        tabBarVC = UITabBarController()
        tabBarVC.setUpTeachatUI()
        
        let viewControllers: [UIViewController] = [
////            MenuVC(nibName: "MenuVC", bundle: nil),
////            AppointmentVC(nibName: "AppointmentVC", bundle: nil),
            AnnouncementVC(apiDelegate: APIManager(), dataDelegate: DataManager(), announcementDelegate: JSONManager()),
////            MessageVC(nibName: "MessageVC", bundle: nil),
////            NotificationVC(nibName: "NotificationVC", bundle: nil)
        ]
//
        var navigationVCs = [UINavigationController]()
//
        for viewController in viewControllers {
//
////            if let menuVC = viewController as? MenuVC {
////                menuVC.delegate = self
////            }
//            
            let navigationVC = UINavigationController(rootViewController: viewController)
            navigationVC.setUpNavUI()
            navigationVCs.append(navigationVC)
        }
//
        tabBarVC.setViewControllers(navigationVCs, animated: false)
//        tabBarVC.selectedIndex = 2
    }
    
    func setUpLoginVC() {
        loginVC = LoginVC(
            apiDelegate: APIManager(),
            userDelegate: JSONManager(),
            loginDelegate: self,
            saveDelegate: DataManager()
        )
    }
}

// MARK: - LoginDelegate Function
extension AppDelegate: LoginDelegate {
    
    func changeRootViewToTabBarVC() {
        setUpTabBarVC()
        
        UIView.transition(
            with: self.window!,
            duration: 0.5,
            options: UIViewAnimationOptions.transitionCrossDissolve,
            animations: { () -> Void in
                self.window?.rootViewController = self.tabBarVC
            },
            completion: nil
        )
        
        loginVC = nil
        
    }
}

// MARK: - LogoutDelegate Function
extension AppDelegate: LogoutDelegate {
    
    func changeRootViewToLoginVC() {
        setUpLoginVC()
        
        UIView.transition(
            with: self.window!,
            duration: 0.5,
            options: UIViewAnimationOptions.transitionCrossDissolve,
            animations: { () -> Void in
                self.window?.rootViewController = self.loginVC
            },
            completion: nil
        )
        
        tabBarVC = nil
        
    }
}

