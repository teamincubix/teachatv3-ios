//
//  DataProviding.swift
//  teachat3
//
//  Created by Julio Alorro on 11/14/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol DataProviding {

    associatedtype Object
    
    func numberOfSections() -> Int
    
    func numberOfRows(inSection: Int) -> Int
    
    func object(at indexPath: IndexPath) -> Object

}
