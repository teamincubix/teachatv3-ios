//
//  AnnouncementCell.swift
//  teachat3
//
//  Created by Julio Alorro on 10/3/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class AnnouncementCell: UITableViewCell {
    
    // MARK: - Delegate Properties
    var cacheDelegate: DataCaching!
    var imageDelegate: ImageFetching!
    weak var announcementEditingDelegate: AnnouncementEditing!
    
    // MARK: - Subviews
    private let announcementView: UIView = UIView()
    
    fileprivate lazy var titleStackView: UIStackView = { [unowned self] in
        let stackView: UIStackView = UIStackView(arrangedSubviews: [self.titleLabel, self.buttonView])
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillProportionally
        stackView.spacing = 5.0
        return stackView
    }()
    
    fileprivate let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.announcementTitle)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.left
        return label
    }()
    
    fileprivate let buttonView: UIView = UIView()
    
    fileprivate let button: UIButton = {
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(TeachatUI.Image.downArrow, for: UIControlState.normal)
        button.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    fileprivate lazy var subtitleStackView: UIStackView = { [unowned self] in
        let stackView: UIStackView = UIStackView(arrangedSubviews: [self.authorLabel, self.dateLabel])
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillProportionally
        stackView.spacing = 0.0
        return stackView
    }()
    
    fileprivate let authorLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.announcementSubtitle)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = TeachatUI.Color.lightGrayFont
        label.textAlignment = NSTextAlignment.left
        return label
    }()
    
    fileprivate let dateLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.announcementSubtitle)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = TeachatUI.Color.lightGrayFont
        label.textAlignment = NSTextAlignment.right
        return label
    }()
    
    fileprivate let bodyLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.announcementBody)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.left
        label.numberOfLines = 0
        return label
    }()
    
    fileprivate let schoolImageView: UIImageView = {
        let imageView = UIImageView(image: TeachatUI.Image.announcement)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        return imageView
    }()
    
    // MARK: - Stored Properties
    fileprivate var indexPath: IndexPath!
    var userID: Int!
    
    // MARK: - Custom Initialization
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.setSubviewsForAutoLayout([self.announcementView, self.schoolImageView])
        contentView.backgroundColor = UIColor.white
        announcementView.setSubviewsForAutoLayout([self.titleStackView, self.subtitleStackView, self.bodyLabel])
        buttonView.setSubviewForAutoLayout(button)
        
        schoolImageView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self.contentView)
            make.top.equalTo(self.contentView).offset(10.0)
            make.height.equalTo(50.0)
            make.width.equalTo(self.schoolImageView.snp.height)
        }
        
        announcementView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self.schoolImageView.snp.trailing)
            make.trailing.equalTo(self.contentView)
            make.top.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView)
        }
        
        titleStackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self.announcementView).offset(10.0)
            make.trailing.equalTo(self.announcementView).offset(-10.0)
            make.top.equalTo(self.announcementView).offset(10.0)
            make.height.equalTo(self.subtitleStackView.snp.height)
        }
        
        buttonView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.width.equalTo(self.titleLabel.snp.width).multipliedBy(0.10)
        }
        
        button.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(self.button.snp.width)
            make.height.equalTo(10.0)
            make.centerX.equalTo(self.buttonView.snp.centerX)
            make.centerY.equalTo(self.buttonView.snp.centerY)
        }
        
        subtitleStackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self.announcementView).offset(10.0)
            make.trailing.equalTo(self.announcementView).offset(-10.0)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5.0)
        }
        
        bodyLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self.announcementView).offset(10.0)
            make.trailing.equalTo(self.announcementView).offset(-10.0)
            make.bottom.equalTo(self.announcementView).offset(-10.0)
            make.top.equalTo(self.subtitleStackView.snp.bottom).offset(7.5)
        }
        
        button.addTarget(self, action: #selector(buttonPressed), for: UIControlEvents.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - TargetAction Functions 
fileprivate extension AnnouncementCell {
    @objc func buttonPressed() {
        announcementEditingDelegate.editAnnouncement(at: self.indexPath)
    }
}

// MARK: - Configurable Cell Protocol
extension AnnouncementCell: ConfigurableCell {
    static var identifier: String  = "AnnouncementCell"
    
    func configure(with announcement: Announcement, at indexPath: IndexPath) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        titleLabel.text = announcement.title
        dateLabel.text = dateFormatter.string(from: announcement.publishDate)
        authorLabel.text = announcement.author.fullName
        bodyLabel.text = announcement.body
        
        self.indexPath = indexPath
        
        switch self.userID != announcement.userID {
            case true:
                self.button.isHidden = true
            
            case false:
                self.button.isHidden = false
        }
        
        let schoolLogo = announcement.schoolLogo as NSString
        
        if let imageData = cacheDelegate.dataCache.object(forKey: schoolLogo) as? Data {
            
            schoolImageView.image = UIImage(data: imageData)
            
        } else {
            
            imageDelegate.fetchImage(with: announcement.schoolLogo) { [unowned self]
                (result: Result) in
                switch result {
                    case .Success(let imageData):
                    
                        self.cacheDelegate.dataCache.setObject(imageData as NSData, forKey: schoolLogo)
                    
                        DispatchQueue.main.async { [unowned self] in
                            self.schoolImageView.image = UIImage(data: imageData)
                        }
                    
                    case .Error(let error):
                        guard let error = error as? NetworkingError else { print("Error at \(self) line 198"); return }
                        print(error)
                }
            }
        }
    }
}
