//
//  RegistrationView.swift
//  teachat3
//
//  Created by Julio Alorro on 9/21/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class RegisterView: UIView {
    fileprivate enum Text: String {
        case country = "Select Country"
        case state = "Select State/Province"
        case school = "Select School"
        case firstName = "First Name"
        case lastName = "Last Name"
        case email = "Email Address"
        case password = "Password"
        case confirmPassword = "Confirm Password"
        case register = "Register"
        case or = "OR"
        case facebook = "Connect with Facebook"
        case login = "Already have an account? Log in"
    }
    
    private enum Role: String {
        case parent, teacher
    }
    
    fileprivate enum Title: String {
        case mr, ms, mrs
    }
    
    fileprivate let background: UIImageView = {
        let imageView = UIImageView()
        imageView.image = TeachatUI.Image.background
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        return imageView
    }()
    
    fileprivate let logo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = TeachatUI.Image.register
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        return imageView
    }()
    
    let roleSegmentControl: UISegmentedControl = {
        let control = UISegmentedControl(items: [Role.teacher.rawValue.capitalized, Role.parent.rawValue.capitalized])
        control.tintColor = TeachatUI.Color.green
        control.backgroundColor = TeachatUI.Color.lightGray
        control.selectedSegmentIndex = 0
        control.setFont(to: UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button))
        control.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return control
    }()
    
    let titleSegmentControl: UISegmentedControl = {
        let control = UISegmentedControl(items: [Title.mr.rawValue.capitalized, Title.ms.rawValue.capitalized, Title.mrs.rawValue.capitalized])
        control.tintColor = TeachatUI.Color.green
        control.backgroundColor = TeachatUI.Color.lightGray
        control.selectedSegmentIndex = 0
        control.setFont(to: UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button))
        control.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return control
    }()
    
    lazy var scrollView: UIScrollView = {  [unowned self] in
        let scrollView = UIScrollView()
        scrollView.contentSize = CGSize(width: self.bounds.width, height: self.bounds.height * self.contentHeight)
        return scrollView
    }()
    
    lazy var scrollContentView: UIView = {
        let contentView = UIView()
        contentView.backgroundColor = UIColor.clear
        return contentView
    }()
    
    var centerLogoConstraint: Constraint!
    
    lazy var countryTextField: TeachatTextField = { [unowned self] in
        let textField = TeachatTextField()
        textField.placeholder = Text.country.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.inputView = self.countryPicker
        
        let imageView = UIImageView(image: TeachatUI.Image.location)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    lazy var stateTextField: TeachatTextField = { [unowned self] in
        let textField = TeachatTextField()
        textField.placeholder = Text.state.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.inputView = self.statePicker
        
        let imageView = UIImageView(image: TeachatUI.Image.location)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    lazy var schoolTextField: TeachatTextField = { [unowned self] in
        let textField = TeachatTextField()
        textField.placeholder = Text.state.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.inputView = self.schoolPicker
        
        let imageView = UIImageView(image: TeachatUI.Image.location)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let countryPicker: UIPickerView = UIPickerView()
    let statePicker: UIPickerView = UIPickerView()
    let schoolPicker: UIPickerView = UIPickerView()
    
    fileprivate lazy var stackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: self.stackViewSubviews)
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 7.5
        return stackView
    }()
    
    let firstNameTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.firstName.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        
        let imageView = UIImageView(image: TeachatUI.Image.name)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let lastNameTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.lastName.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        
        let imageView = UIImageView(image: TeachatUI.Image.name)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let emailTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.email.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.setUpKeyboard(UITextField.KeyboardType.email, returnKeyType: UIReturnKeyType.next)
        
        let imageView = UIImageView(image: TeachatUI.Image.email)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let passwordTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.password.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.setUpKeyboard(UITextField.KeyboardType.password, returnKeyType: UIReturnKeyType.next)
        
        let imageView = UIImageView(image: TeachatUI.Image.password)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let confirmPasswordTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.confirmPassword.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.setUpKeyboard(UITextField.KeyboardType.password, returnKeyType: UIReturnKeyType.next)
        
        let imageView = UIImageView(image: TeachatUI.Image.password)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let registerButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle(Text.register.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.green
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()
    
    fileprivate let orLabel: UILabel = {
        let label = UILabel()
        label.text = Text.or.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    let facebookButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle(Text.facebook.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.facebookBlue
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle(Text.login.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.backgroundColor = TeachatUI.Color.lightGray
        indicator.color = TeachatUI.Color.green
        indicator.hidesWhenStopped = true
        indicator.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return indicator
    }()
    
    private lazy var stackViewSubviews: [UIView] = [
        self.countryTextField, self.stateTextField, self.schoolTextField,
        self.firstNameTextField, self.lastNameTextField, self.emailTextField,
        self.passwordTextField, self.confirmPasswordTextField, self.registerButton,
        self.orLabel, self.facebookButton, self.loginButton
    ]
    
    fileprivate lazy var textFields: [TeachatTextField] = [
        self.countryTextField, self.stateTextField,
        self.schoolTextField, self.firstNameTextField,
        self.lastNameTextField, self.emailTextField,
        self.passwordTextField, self.confirmPasswordTextField
    ]
    
    
    private lazy var subviewsArray: [UIView] = [self.background, self.scrollView]
    
    private lazy var scrollContentViewSubviews: [UIView] = [self.logo, self.roleSegmentControl, self.titleSegmentControl, self.stackView, self.activityIndicator]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setSubviewsForAutoLayout(subviewsArray)
        scrollView.setSubviewForAutoLayout(scrollContentView)
        scrollContentView.setSubviewsForAutoLayout(scrollContentViewSubviews)
        
        background.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.edges.equalTo(self)
        }
        
        scrollView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.edges.equalTo(self)
        }

        //      Set scrollContentView equal widths to root View
        scrollContentView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.edges.equalTo(self.scrollView)
            make.width.equalTo(self)
        }
        
        logo.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            self.centerLogoConstraint = make.centerX.equalTo(self).constraint
            make.height.equalTo(self.snp.height).multipliedBy(0.10)
            make.width.equalTo(self.logo.snp.height).multipliedBy(4.5)
        }
        
        roleSegmentControl.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.logo.snp.bottom).offset(30.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
        }

        titleSegmentControl.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.roleSegmentControl.snp.bottom).offset(7.5)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
        }

        loginButton.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
        }

        stackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.titleSegmentControl.snp.bottom).offset(7.5)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.bottom.equalTo(self.scrollContentView).offset(-15.0)
            //            make.bottom.equalTo(self).offset(-15.0)
        }
        
        activityIndicator.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.width.equalTo(self.activityIndicator.snp.height)
            make.height.equalTo(self).multipliedBy(0.10)
            make.center.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setUpSecondarySettings()
        
    }
    
    var teacherIsSelected: Bool = false {
        didSet {
            switch teacherIsSelected {
                case true:
                    contentHeight = 1.4
                
                case false:
                    contentHeight = 1.2
            }
            
            let height = self.bounds.height * self.contentHeight
            scrollView.contentSize = CGSize(width: self.bounds.width, height: height)
            
            if roleControlWasPressed {
                UIView.animate(withDuration: 0.125) { [unowned self] in
                    self.scrollContentView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
                        make.edges.equalTo(self.scrollView)
                        make.width.equalTo(self)
                        make.height.equalTo(height)
                    }
                    self.layoutIfNeeded()
                }
            }
        }
    }
    
    fileprivate var roleControlWasPressed: Bool = false
    fileprivate var contentHeight: CGFloat = 1.4
}

// MARK: - Helper Functions
private extension RegisterView {
    func setUpSecondarySettings() {
        switch roleControlWasPressed {
            case false:
                scrollContentView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
                    make.edges.equalTo(self.scrollView)
                    make.width.equalTo(self)
                    make.height.equalTo(self.bounds.height * self.contentHeight)
                }
                
                for textField in textFields {
                    
                    guard let leftView = textField.leftView else { continue }
                    
                    leftView.frame.size = CGSize(width: textField.iconWidth, height: textField.iconHeight)
                    
                }
                
                facebookButton.createSubImage(with: TeachatUI.Image.facebook, padding: TeachatUI.Icon.padding, alpha: 1.0)
                
                roleControlWasPressed = true
            
            case true:
                break
            
        }
    }
}
