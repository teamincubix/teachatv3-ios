//
//  TypeExtensions.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

extension String {
    internal var isAlphabetic: Bool {
        let alphaRegex = "^[a-zA-Z]+$"
        let range = self.range(of: alphaRegex, options: NSString.CompareOptions.regularExpression)
        if range != nil { return true } else { return false }
    }
    
    internal var isValidEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let range = self.range(of: emailRegex, options: NSString.CompareOptions.regularExpression)
        if range != nil { return true } else { return false }
    }
    
    internal var isValidPhoneNum: Bool {
        let phoneRegex = "^[0-9]{3}-[0-9]{3}-[0-9]{4}$"
        let range = self.range(of: phoneRegex, options: NSString.CompareOptions.regularExpression)
        if range != nil { return true } else { return false }
    }
    
    internal var base64Encoded: String {
        let encodedData: Data? = self.data(using: String.Encoding.utf8)
        let base64String: String? = encodedData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return base64String!
    }
    
    internal var base64Decoded: String? {
        let base64Regex = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$"
        let range = self.range(of: base64Regex, options: NSString.CompareOptions.regularExpression)
        if range != nil {
            let decodedData: Data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))!
            let decodedString: String? = NSString(data: decodedData, encoding: String.Encoding.utf8.rawValue) as? String
            return decodedString
        } else {
            return nil
        }
    }
}
