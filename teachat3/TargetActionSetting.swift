//
//  TargetActionSetting.swift
//  teachat3
//
//  Created by Julio Alorro on 10/13/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

protocol TargetActionSetting {

    func setUpTargetActions(_ dictionary: [UIControl: Selector])

}

extension TargetActionSetting where Self: UIViewController {
    
    func setUpTargetActions(_ dictionary: [UIControl: Selector]) {
        for (control, action) in dictionary {
            
            let controlEvent: UIControlEvents
            
            switch control {
                
            case is UISegmentedControl, is UIDatePicker, is UIRefreshControl:
                controlEvent = .valueChanged
                
            default:
                controlEvent = .touchUpInside
            }
            
            control.addTarget(self, action: action, for: controlEvent)
        }
    }
}
