//
//  Result.swift
//  teachat3
//
//  Created by Julio Alorro on 11/7/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

enum Result<T> {
    case Success(T)
    case Error(Error)
}
