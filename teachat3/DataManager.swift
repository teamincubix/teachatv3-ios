//
//  DataManager.swift
//  teachat3
//
//  Created by Julio Alorro on 9/19/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

private let appIdentifier = Bundle.main.object(forInfoDictionaryKey: "AppIdentifierPrefix") as! String

struct DataManager {
    
    fileprivate let keychain: KeychainWrapper = KeychainWrapper(
        serviceName: "TeachatKeychainService",
        accessGroup: "\(appIdentifier)com.incubixtech.teachat3"
    )
    
    var dataCache: NSCache<NSString, NSData> = {
        let cache: NSCache<NSString, NSData> = NSCache()
        cache.countLimit = 10
        cache.name = "ImageCache"
        return cache
    }()
    
}

extension DataManager: DataCaching { }

extension DataManager: DataSaving {
    
    func save(data: Saveable) -> Bool {
        
        let dictionary: NSDictionary = data.dictionaryRepresentation as NSDictionary
        
        return keychain.set(dictionary, forKey: JSONKey.userData.rawValue)
    
    }
}

extension DataManager: DataRemoving {
    
    func removeData() -> Bool {
    
        return keychain.removeObject(forKey: JSONKey.userData.rawValue)
    
    }
}

extension DataManager: DataGetting {
    
    var hasValue: Bool {
        return keychain.hasValue(forKey: JSONKey.userData.rawValue)
    }
    
    func getData() -> [String: Any]? {
        
        let object = keychain.object(forKey: JSONKey.userData.rawValue)
        guard let data = object as? [String: Any] else { print("No Data"); return nil }
        return data
    
    }
}

