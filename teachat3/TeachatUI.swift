//
//  TeachatUI.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit

enum TeachatUI {
    enum Color {
        static let lightGray: UIColor = UIColor(red: 232, green: 232, blue: 232)
        static let navBar: UIColor = UIColor(red: 48, green: 65, blue: 72)
        static let facebookBlue: UIColor = UIColor(red: 55, green: 94, blue: 155)
        static let green: UIColor = UIColor(red: 22, green: 158, blue: 93)
        static let gray: UIColor = UIColor(red: 226, green: 226, blue: 226)
        static let red: UIColor = UIColor(red: 204, green: 0, blue: 51)
//        static let male: UIColor = UIColor(red: 65, green: 131, blue: 215)
        static let warningRed: UIColor = UIColor(red: 255, green: 204, blue: 204)
        static let lightGrayFont: UIColor = UIColor(red: 136, green: 136, blue: 136)
        static let teal: UIColor = UIColor(red: 38, green: 166, blue: 154)
        static let placeHolderGray: UIColor = UIColor(red: 199, green: 199, blue: 205)
    }
    
    enum View {
        static let cornerRadius: CGFloat = 5.0
        static let ratio: CGFloat = 0.07
    }
    enum Icon {
        static let padding: CGFloat = 10.0
        static let textFieldAlpha: CGFloat = 0.70
    }
    enum RobotoFont: String {
        case regular = "Roboto-Regular"
        case bold = "Roboto-Bold"
        case italic = "Roboto-Italic"
    }
    
    enum FontSize {
        static let textField: CGFloat = 17.0
        static let button: CGFloat = 17.0
        static let announcementTitle: CGFloat = 15.0
        static let announcementSubtitle: CGFloat = 12.0
        static let announcementBody: CGFloat = 14.0
    }
    
    enum Image {
        static let address: UIImage? = UIImage(named: "address")
        static let announcement: UIImage? = UIImage(named: "announcement")
        static let appointment: UIImage? = UIImage(named: "appointment")
        static let background: UIImage? = UIImage(named: "background")
        static let confirmPassword: UIImage? = UIImage(named: "confirm_password")
        static let downArrow: UIImage? = UIImage(named: "down_arrow")
        static let email: UIImage? = UIImage(named: "email")
        static let facebook: UIImage? = UIImage(named: "facebook")
        static let forgotPassword: UIImage? = UIImage(named: "forgot_password")
        static let location: UIImage? = UIImage(named: "location")
        static let menu: UIImage? = UIImage(named: "menu")
        static let message: UIImage? = UIImage(named: "message")
        static let mobile: UIImage? = UIImage(named: "mobile")
        static let name: UIImage? = UIImage(named: "name")
        static let notification: UIImage? = UIImage(named: "notification")
        static let password: UIImage? = UIImage(named: "password")
        static let post: UIImage? = UIImage(named: "post")
        static let register: UIImage? = UIImage(named: "register")
        static let teachat: UIImage? = UIImage(named: "teachat")
        static let teachatNavbar: UIImage? = UIImage(named: "teachat_navbar")
        static let zipcode: UIImage? = UIImage(named: "zipcode")
    }
    
    enum NavBar {
        static let attributes = [NSFontAttributeName: UIFont(name: RobotoFont.regular.rawValue, size: 15.0)!]
        static let logoName = "Logo"
    }
    
    enum AnimationKey {
        static let position = "position"
    }
}
