//
//  JSONKey.swift
//  teachat3
//
//  Created by Julio Alorro on 9/19/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

enum JSONKey: String {
    
    case data
    case status
    case result
    
    case gender
    case birthdate
    case user
    case token
    case email
    case city
    case firstName = "first_name"
    case middleName = "middle_name"
    case lastName = "last_name"
    case addressOne = "address_one"
    case addressTwo = "address_two"
    case cell = "contact_cell"
    case home = "contact_home"
    case work = "contact_work"
    case schoolID = "school_id"
    case countryID = "country_id"
    case stateID = "state_id"
    case roleID = "role_id"
    case title
    case password
    case children
    case child
    case studentDetails = "student_details"
    
    // Location Data Related
    case country
    case name
    case id
    case state
    case stateName = "state_name"
    case schools = "schools"
    case schoolName = "school_name"
    case schoolLogo = "school_logo"
    
    // UserData
    case userData
    
    // Announcement Data Related
    case userID = "user_id"
    case publishDate = "publish_on"
    case expireDate = "expiration_date"
    case announcement
    case postedBy = "posted_by"
    case teacherSubjectID = "teacher_subject_id"
    case announcementSubjects = "announcement_subject"
    case teacherSubject = "teacher_subject"
    
    // Child Data Related
    case school
    case section
    case gradeID = "grade_id"
    case grade
    case description = "description"
    
    // Subject Data Related
    case subject
    case topic
}
