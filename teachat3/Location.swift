//
//  LocationData.swift
//  teachat3
//
//  Created by Julio Alorro on 9/23/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Location {
    
    let countries: [Country]
    let states: [Country: [State]]
    let schools: [State: [School]]

}
