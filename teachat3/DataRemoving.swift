//
//  RemoveDataDelegate.swift
//  teachat3
//
//  Created by Julio Alorro on 9/28/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol DataRemoving {
    
    func removeData() -> Bool
    
}
