//
//  AuthenticationDelegate.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol APIQuerying {
    
    func query(endpoint: APIManager.Endpoint, parameters: [String: Any]?, completionHandler: @escaping (Result<Data>) -> Void)
    
}
