//
//  School.swift
//  teachat3
//
//  Created by Julio Alorro on 10/10/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct School {
    let name: String
    let id: Int
    let logo: String
    let state: State
    
    init(name: String, id: Int, logo: String, state: State) {
        self.name = name
        self.id = id
        self.logo = logo
        self.state = state
    }
    
    init?(dictionary: [String: Any]) {
        guard let name = dictionary[JSONKey.schoolName.rawValue] as? String,
            let id = dictionary[JSONKey.id.rawValue] as? Int,
            let logo = dictionary[JSONKey.schoolLogo.rawValue] as? String,
            
            //State
            let stateDict = dictionary[JSONKey.state.rawValue] as? [String: Any],
            let state = State(dictionary: stateDict)
            
            else { print("School initialization failed"); return nil }
        
        self.name = name
        self.id = id
        self.logo = logo
        self.state = state
    }
}

extension School: Saveable {
    var dictionaryRepresentation: [String: Any] {
        
        return [
            JSONKey.schoolName.rawValue: self.name,
            JSONKey.id.rawValue: self.id,
            JSONKey.schoolLogo.rawValue: self.logo,
            JSONKey.state.rawValue: self.state.dictionaryRepresentation
        ]
    }
}
