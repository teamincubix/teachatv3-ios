//
//  State.swift
//  teachat3
//
//  Created by Julio Alorro on 10/10/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct State {
    let name: String
    let id: Int
    let country: Country
    
    init(name: String, id: Int, country: Country) {
        self.name = name
        self.id = id
        self.country = country
    }
    
    init?(dictionary: [String: Any]) {
        guard let name = dictionary[JSONKey.stateName.rawValue] as? String,
            let id = dictionary[JSONKey.id.rawValue] as? Int,
            let countryDict = dictionary[JSONKey.country.rawValue] as? [String: Any],
            let country = Country(dictionary: countryDict)
            else { print("State initialization failed"); return nil }
        
        self.name = name
        self.id = id
        self.country = country
    }
}

extension State: Hashable {
    var hashValue: Int {
        return name.hashValue ^ id.hashValue ^ country.hashValue
    }
    
    public static func ==(lhs: State, rhs: State) -> Bool {
        return lhs.name == rhs.name && lhs.id == rhs.id && lhs.country == rhs.country
    }
    
}

extension State: Saveable {
    var dictionaryRepresentation: [String: Any] {
        return [
            JSONKey.stateName.rawValue: self.name,
            JSONKey.id.rawValue: self.id,
            JSONKey.country.rawValue: self.country.dictionaryRepresentation
        ]
    }
}
