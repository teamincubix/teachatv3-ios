//
//  Fadeable.swift
//  teachat3
//
//  Created by Julio Alorro on 11/9/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

protocol Fadeable {}

extension Fadeable where Self: UIView {
    
    func fade(rootView: UIView) {
        
        UIView.animate(
            withDuration: 0.3,
            delay: 0.0,
            options: UIViewAnimationOptions.curveEaseIn,
            animations: { [unowned self] in
                
                self.alpha = 1.0
                
            }) { (isComplete: Bool) in
                if isComplete {
                    UIView.animate(
                        withDuration: 0.3,
                        delay: 2.0,
                        options: UIViewAnimationOptions.curveEaseOut,
                        animations: { [unowned self] in
                     
                            self.alpha = 0.0
                     
                    }) { (isComplete: Bool) in
                        if isComplete {
                            UIView.animate(withDuration: 0.075) {
                        
                                self.snp.remakeConstraints{ (make: ConstraintMaker) in
                                    
                                    make.height.equalTo(0.0)
                                    
                                }
                                
                                rootView.layoutIfNeeded()
                                
                            }
                        }
                }
            }
        }
    }
    
    func fade() {
        
        UIView.animate(
            withDuration: 0.3,
            delay: 0.0,
            options: UIViewAnimationOptions.curveEaseIn,
            animations: { [unowned self] in
                
                self.alpha = 1.0
                
        }) { (isComplete: Bool) in
            if isComplete {
                UIView.animate(
                    withDuration: 0.3,
                    delay: 2.0,
                    options: UIViewAnimationOptions.curveEaseOut,
                    animations: { [unowned self] in
                        
                        self.alpha = 0.0
                        
                }, completion: nil)
                
            }
        }
    }
}
