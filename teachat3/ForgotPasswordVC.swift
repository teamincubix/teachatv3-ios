//
//  ForgotPasswordVC.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class ForgotPasswordVC: UIViewController, InteractionHandling, AlertHandling, TargetActionSetting {
    
    // MARK: - Delegate Properties
    fileprivate let apiDelegate: APIQuerying
    
    // MARK: Custom Initializer
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        // ForgotPasswordVC should not know that APIManager is of type APIDelegate
        // also with JSONManager being of type ForgotPasswordDelegate
        // Fix in the future when a solution is thought of
        apiDelegate = APIManager()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = ForgotPasswordView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewController()
        self.setUpTextfieldDelegate()
        self.registerForKeyboardNotifications()
        self.setUpTargetActions([
            self.getPasswordButton: #selector(getPasswordButtonPressed),
            self.goBackButton: #selector(dismissForgotPasswordVC)
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        AnimationManager.animator.animateConstraintsOnScreen(
//            constraints: [self.centerLogoConstraint],
//            delay: 1,
//            position: AnimationManager.OffScreenPosition.right
//        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        AnimationManager.animator.removeAnimations([self.centerLogoConstraint])
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.emailTextField.resignFirstResponder()
    }
    
    deinit {
        print("\(self) was deallocated")
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate lazy var alert: UIAlertController = {
        return UIAlertController(title: nil, message: nil, preferredStyle: .alert)
    }()
    
    fileprivate lazy var buttons: [UIButton] = [self.getPasswordButton, self.goBackButton]
    
}

// MARK: - Computed Properties (and IBOutlets)
extension ForgotPasswordVC {
    fileprivate weak var forgotPasswordView: ForgotPasswordView! { return self.view as! ForgotPasswordView }
    weak var emailTextField: TeachatTextField! { return self.forgotPasswordView.emailTextField }
    weak var getPasswordButton: TeachatButton! { return self.forgotPasswordView.getPasswordButton }
    weak var goBackButton: TeachatButton! { return self.forgotPasswordView.goBackButton }
    fileprivate weak var activityIndicator: UIActivityIndicatorView! { return self.forgotPasswordView.activityIndicator }
    fileprivate weak var centerLogoConstraint: Constraint! { return self.forgotPasswordView.centerLogoConstraint }
    weak var errorLabel: TeachatLabel! { return self.forgotPasswordView.errorLabel }
    
}

// MARK: - Helper Functions
fileprivate extension ForgotPasswordVC {
    
    func setUpViewController() {
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }
    
    func setUpTextfieldDelegate() {
        self.emailTextField.delegate = self
    }
    
    @objc func keyboardWasShown(_: Notification) {
        self.changeUserInteraction(of: self.buttons, to: false)
    }
    
    @objc func keyboardWasDismissed(_: Notification) {
        self.changeUserInteraction(of: self.buttons, to: true)
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasDismissed(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    func animate(shakeable: Shakeable, error: DisplayableError) {
        self.errorLabel.text = "\(error.title): \(error.localizedDescription)"
        self.errorLabel.fade()
        
        shakeable.shake()
    }
}

// MARK: - Textfield Delegate Function
extension ForgotPasswordVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField.text != "" {
            self.getPasswordButtonPressed()
        }
        
        return false
    }
}

// MARK: - Target Action Functions (IBActions)
extension ForgotPasswordVC {
    
    // MARK: - Alert Text Enum
    enum AlertText: String {
        case title = "Success!"
        case message = "Your password has been reset. Please check your email."
    }
    
    enum ErrorText: String {
        case invalidEmail = "Email is not valid"
        case missing = "You must put an email"
    }
    
    @objc func dismissForgotPasswordVC() {
        self.goBackButton.spring()
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func getPasswordButtonPressed() {
        
        getPasswordButton.spring()
        
        if let email = emailTextField.text, !email.isEmpty {
            
            guard email.isValidEmail else {
                self.animate(
                    shakeable: self.emailTextField,
                    error: TeachatError.invalidInput(ErrorText.invalidEmail.rawValue)
                )
                return
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.activityIndicator.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            let parameters = [JSONKey.email.rawValue: email]
            
            // Query the API
            self.apiDelegate.query(endpoint: APIManager.Endpoint.passwordReset, parameters: parameters) {
                [unowned self] (result: Result) in
                DispatchQueue.main.async {
                    
                    switch result {
                        case .Success:
                            
                            self.alert.title = AlertText.title.rawValue
                            self.alert.message = AlertText.message.rawValue
                    
                            let successAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                                [unowned self] (_: UIAlertAction) in
                                self.presentingViewController?.dismiss(animated: true, completion: nil)
                                self.dismissForgotPasswordVC()
                            }
                    
                            self.present(alert: self.alert, action: successAction)
                
                        case .Error(let error):
                    
                            guard let error = error as? DisplayableError else { print("Error at LoginVC line 205"); return }
                            self.animate(shakeable: self.emailTextField, error: error)
        
                    }
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                }
            }
            
        } else {

            self.animate(shakeable: self.emailTextField, error: TeachatError.missingInput(ErrorText.missing.rawValue))

        }
    }
}
