//
//  DataPicking.swift
//  teachat3
//
//  Created by Julio Alorro on 10/12/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol SubjectPicking: class {

    var pickedSubjects: [Subject] { get }
    
    func addToArray(subject: Subject)
    
    func removeFromArray(subject: Subject)
    
    func discardArray()
    
}
