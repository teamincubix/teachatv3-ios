//
//  UserInteractionHandler.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

protocol InteractionHandling {
    
    func changeUserInteraction(of controls: [UIControl], to bool: Bool)
    
}

extension InteractionHandling {
    
    func changeUserInteraction(of controls: [UIControl], to bool: Bool) {
        for control in controls {
            control.isEnabled = bool
        }
    }
}
