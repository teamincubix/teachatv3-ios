//
//  TeachatError.swift
//  teachat3
//
//  Created by Julio Alorro on 11/7/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

enum TeachatError: DisplayableError {
    case invalidCredentials(String)
    case invalidInput(String)
    case missingInput(String)
    case invalidRole
    case appSide(String)
    
    var localizedDescription: String {
        switch self {
        case .invalidCredentials(let string):
            return string
        case .invalidInput(let string):
            return string
        case .missingInput(let string):
            return string
        case .invalidRole:
            return "Account role must be teacher or parent"
        case .appSide(let string):
            return string
            
        }
    }
    
    var title: String {
        switch self {
        case .invalidCredentials:
            return "Invalid Credentials"
        case .invalidInput:
            return "Invalid Input"
        case .missingInput:
            return "Missing Input"
        case .invalidRole:
            return "Invalid Account Role"
        case .appSide:
            return "Application Error"
        }
    }
}
