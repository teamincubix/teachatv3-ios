//
//  Subject.swift
//  teachat3
//
//  Created by Julio Alorro on 10/11/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

struct Subject {
    let id: Int
    let name: String
    let grade: Grade
    let topic: Topic
}

extension Subject: Equatable {
    public static func ==(lhs: Subject, rhs: Subject) -> Bool {
        return
            lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.grade == rhs.grade &&
            lhs.topic == rhs.topic
    }
}
