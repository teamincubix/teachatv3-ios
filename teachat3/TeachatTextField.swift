//
//  TeachatTextField.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

class TeachatTextField: UITextField, Shakeable {

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return newBounds(bounds)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return newBounds(bounds)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
}

// MARK: - Helper Functions
private extension TeachatTextField {
    
    func newBounds(_ bounds: CGRect) -> CGRect {
        
        var newBounds = bounds
        newBounds.origin.x += self.frame.width / 7
        newBounds.origin.y += 0
        newBounds.size.height -= 0
        newBounds.size.width -= self.frame.width / 7
        return newBounds
    }
}
