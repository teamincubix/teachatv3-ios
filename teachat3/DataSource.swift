//
//  AnnouncementsDataSource.swift
//  teachat3
//
//  Created by Julio Alorro on 11/14/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

class DataSource<Provider: DataProviding, Cell: UITableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate where Cell: ConfigurableCell, Provider.Object == Cell.Object {
    
    // MARK: - Stored Properties
    var provider: Provider
    weak var tableView: UITableView!
    
    // MARK: - Initializer
    init(provider: Provider, tableView: UITableView) {
        self.provider = provider
        self.tableView = tableView
        super.init()
        
        self.setup()
    }
    
    // MARK: - Instance Methods
    func setup() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
    }
    
    // MARK: - UITableViewDataSource Functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.provider.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.provider.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        let object = provider.object(at: indexPath)
        cell.configure(with: object, at: indexPath)
        return cell
    }
    
}
