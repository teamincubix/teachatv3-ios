//
//  TokenSharing.swift
//  teachat3
//
//  Created by Julio Alorro on 10/12/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol TokenSharing: class {
    
    var token: String! { get }
    
}
