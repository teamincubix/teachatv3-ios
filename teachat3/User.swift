//
//  User.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper


struct User: Saveable {
    let token: String
    let id: Int
    let role: User.Role
    let email: String
    let firstName: String
    let middleName: String?
    let lastName: String
    let gender: String?
    let birthDate: Date?
    let addressOne: String?
    let addressTwo: String?
    let city: String?
//    let state: String
//    let country: String
    
    let cell: String?
    let home: String?
    let work: String?
    let children: [Child]?
    
    // MARK: - Role Enum
    enum Role: Int {
        case teacher = 2
        case parent = 3
        
        init?(rawValue: Int) {
            
            switch rawValue {
                case 2: self = .teacher
                case 3: self = .parent
                default: return nil
            }
        }
    }
    
    
    var dictionaryRepresentation: [String: Any] {
        var dictionary: [String: Any] = [:]
        
        if let middleName = self.middleName {
            dictionary[JSONKey.middleName.rawValue] = middleName
        }
        
        if let gender = self.gender {
            dictionary[JSONKey.gender.rawValue] = gender
        }
        
        if let birthDate = self.birthDate {
            dictionary[JSONKey.birthdate.rawValue] = birthDate
        }
        
        if let addressOne = self.addressOne {
            dictionary[JSONKey.addressOne.rawValue] = addressOne
        }
        
        if let addressTwo = self.addressTwo {
            dictionary[JSONKey.addressTwo.rawValue] = addressTwo
        }
        
        if let city = self.city {
            dictionary[JSONKey.city.rawValue] = city
        }
        
        if let cell = self.cell {
            dictionary[JSONKey.cell.rawValue] = cell
        }
        
        if let home = self.home {
            dictionary[JSONKey.home.rawValue] = home
        }
        
        if let work = self.work {
            dictionary[JSONKey.work.rawValue] = work
        }
        
        if let children = self.children {
            
            var childrenArray: [[String: Any]] = []
            
            for child in children {
        
                childrenArray.append(child.dictionaryRepresentation)
            }
            
            dictionary[JSONKey.children.rawValue] = childrenArray
        }
        
        
        
        dictionary[JSONKey.token.rawValue] = self.token
        dictionary[JSONKey.id.rawValue] = self.id
        dictionary[JSONKey.roleID.rawValue] = self.role.rawValue
        dictionary[JSONKey.email.rawValue] = self.email
        dictionary[JSONKey.firstName.rawValue] = self.firstName
        dictionary[JSONKey.lastName.rawValue] = self.lastName
        
        
        return dictionary
    }
    
    init(token: String, id: Int, roleID: Int, email: String, firstName: String, middleName: String?, lastName: String, gender: String?, birthDate: Date?, addressOne: String?, addressTwo: String?, city: String?, cell: String?, home: String?, work: String?, children: [Child]) {
        
        let role = User.Role(rawValue: roleID)!
        
        // Non Optional
        self.token = token
        self.id = id
        self.role = role
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        
        // Optional
        self.middleName = middleName
        self.gender = gender
        self.birthDate = birthDate
        self.addressOne = addressOne
        self.addressTwo = addressTwo
        self.city = city
        self.cell = cell
        self.home = home
        self.work = work
        self.children = children
    }
    
    
    init?(dictionary: [String: Any]) {
        
        guard let token = dictionary[JSONKey.token.rawValue] as? String, !token.isEmpty,
            let id = dictionary[JSONKey.id.rawValue] as? Int,
            let roleID = dictionary[JSONKey.roleID.rawValue] as? Int,
            let email = dictionary[JSONKey.email.rawValue] as? String, !email.isEmpty,
            let firstName = dictionary[JSONKey.firstName.rawValue] as? String, !firstName.isEmpty,
            let lastName = dictionary[JSONKey.lastName.rawValue] as? String, !lastName.isEmpty
        else {
            return nil
        }
        
        let middleName = dictionary[JSONKey.middleName.rawValue] as? String
        let gender = dictionary[JSONKey.gender.rawValue] as? String
        let birthDate = dictionary[JSONKey.birthdate.rawValue] as? Date
        let addressOne = dictionary[JSONKey.addressOne.rawValue] as? String
        let addressTwo = dictionary[JSONKey.addressTwo.rawValue] as? String
        let city = dictionary[JSONKey.city.rawValue] as? String
        let cell = dictionary[JSONKey.cell.rawValue] as? String
        let home = dictionary[JSONKey.home.rawValue] as? String
        let work = dictionary[JSONKey.work.rawValue] as? String
        let children = dictionary[JSONKey.children.rawValue] as? [[String: Any]]
        
        guard let role = User.Role(rawValue: roleID) else { print("User Role not defined"); return nil }
        
        var childrenArray: [Child] = []
        
        if let children = children {
            
            for childDict in children {
            
                if let child = Child(dictionary: childDict) {
                
                    childrenArray.append(child)
                
                }
            }
        }
        
        // Non Optional
        self.token = token
        self.id = id
        self.role = role
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        
        // Optional
        self.middleName = middleName
        self.gender = gender
        self.birthDate = birthDate
        self.addressOne = addressOne
        self.addressTwo = addressTwo
        self.city = city
        self.cell = cell
        self.home = home
        self.work = work
        self.children = childrenArray
    }
}
