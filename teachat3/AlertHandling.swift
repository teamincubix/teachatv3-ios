//
//  ErrorAlertHandler.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

protocol AlertHandling {
    
    func present(alert: UIAlertController, error: DisplayableError?, action: UIAlertAction?)
    
}

extension AlertHandling where Self: UIViewController {
    
    func present(alert: UIAlertController, error: DisplayableError? = nil, action: UIAlertAction? = nil) {
        
        if let error = error {
            alert.title = error.title
            alert.message = error.localizedDescription
        }
        
        if alert.actions.isEmpty, action == nil {
            alert.addAction(alert.defaultDismissAction)
        } else if let action = action {
            alert.addAction(action)
        }
        
        present(alert, animated: true, completion: nil)
    }
}
