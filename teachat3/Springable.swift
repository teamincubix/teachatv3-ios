//
//  Springable.swift
//  teachat3
//
//  Created by Julio Alorro on 11/8/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

protocol Springable {}

extension Springable where Self: UIView {
    
    func spring() {
        self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(
            withDuration: 1.0,
            delay: 0.0,
            usingSpringWithDamping: 0.25,
            initialSpringVelocity: 6.0,
            options: UIViewAnimationOptions.allowUserInteraction, animations: { [unowned self] in
                self.transform = CGAffineTransform.identity
            },
            completion: nil
        )
    }
}


