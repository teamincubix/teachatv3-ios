//
//  PostAnnouncementVC.swift
//  teachat3
//
//  Created by Julio Alorro on 10/8/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

class PostAnnouncementVC: UIViewController, SubjectPicking, AlertHandling, TargetActionSetting {
    
    // MARK: - Delegate Properties
    fileprivate let apiDelegate: APIQuerying
    fileprivate let subjectDelegate: SubjectCreating
    weak var tokenDelegate: TokenSharing!
    weak var tableDelegate: TableReloading!
    
    // MARK: - Custom Initialization
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        apiDelegate = APIManager()
        subjectDelegate = JSONManager()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Controller Lifecycle Methods
    override func loadView() {
        self.view = PostAnnouncementView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        queryForSubjects()
        setUpTargetActions(targetActionDictionary)
        setUpTextFields()
        registerForKeyboardNotifications()
        setUpNavBar()
        bodyTextView.delegate = self
    }
    
    
    // MARK: - Other View Controller Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let textField = activeTextField {
            
            textField.resignFirstResponder()
            
        } else if let textView = activeTextView {
            
            textView.resignFirstResponder()
            
        }
    }
    
    // MARK: - Deinitialization
    deinit {
        print("Deallocation \(self)")
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Text Enum
    fileprivate enum Text: String {
        case cancel = "Cancel"
        case edit = "Edit"
        case editTitle = "Edit selected subjects?"
        case noExpiration = "9999-12-31"
        case allSubjects = "All Subjects Selected"
        case subjectsSelected = "Subject(s) Selected"
        case editAnnouncement = "Edit Announcement"
        case allSubjectName = "All Subjects"
    }
    
    // MARK: - Error Text Enum
    fileprivate enum ErrorText: String {
        case announcementMissing = "Announcement post is missing required information"
        case announcementEmpty = "Announcement is empty"
        case publishDate = "Publish date must be earlier than expiration date"
        case subject = "No subjects picked"
    }
    
    //MARK: - Stored Properties
    fileprivate weak var activeTextField: UITextField?
    fileprivate weak var activeTextView: UITextView?
    fileprivate var subjects: [Subject]?
    fileprivate lazy var textFields: [UITextField] = [self.titleTextField, self.publishTextField, self.expireTextField]
    fileprivate lazy var targetActionDictionary: [UIControl: Selector] = [
        self.publishPicker: #selector(publishDateControlPressed), self.expirePicker: #selector(expireDateControlPressed),
        self.announceToButton: #selector(announceToButtonPressed), self.postButton: #selector(postAnnouncement),
        self.segmentedControl: #selector(segmentedControlPressed)
    ]
    
    // MARK: Date Formatter
    fileprivate let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    // MARK: Alert Action Sheet
    fileprivate lazy var alertActionSheet: UIAlertController = { [unowned self] in
        let alert = UIAlertController(title: Text.editTitle.rawValue, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actions: [UIAlertAction] = [
            UIAlertAction(title: Text.edit.rawValue, style: UIAlertActionStyle.default) {
                [unowned self] (_: UIAlertAction) in
            
                // If the subject is "All Subjects" empty out array ( there will only be one subject in array if "All Subjects" was selected
                if let subject = self.pickedSubjects.first, subject.id == 0  {
                    self.discardArray()
                }
            
                self.presentSubjectSelectionVC()
            },
            UIAlertAction(title: Text.cancel.rawValue, style: UIAlertActionStyle.cancel) {
                [unowned self] (_: UIAlertAction) in
        
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        ]
    
        for action in actions {
            alert.addAction(action)
        }
        
        return alert
    }()
    
    // MARK: Alert Controller
    fileprivate lazy var alert: UIAlertController = {
        return UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    }()
    
    // MARK: - Announcement Object - When selected from Announcement Table
    var announcement: Announcement?
    var announcementWasEdited: Bool = false {
        didSet {
            switch announcementWasEdited {
            
            case true:
                postButton.isEnabled = true
                postButtonItem.isEnabled = true
            case false:
                postButton.isEnabled = false
                postButtonItem.isEnabled = false
            }
        }
    }
    
    // MARK: - Keyboard Height Divisor
    let keyboardHeightDivisor: CGFloat = 4.0
    
    // MARK: - QueryType Enum
    fileprivate enum QueryType {
        case post
        case put
    }
    
    // MARK: - SubjectPicking Protocol Stored Property & Functions
    var pickedSubjects: [Subject] = [] {
        didSet {
            if pickedSubjects.isEmpty {
                announceToButton.setTitle(postAnnouncementView.announceToButtonText, for: UIControlState.normal)
                
            } else if pickedSubjects.count == 1, pickedSubjects.first!.id == 0 {
                
                announceToButton.setTitle(Text.allSubjects.rawValue, for: UIControlState.normal)
                
            } else {
                
                announceToButton.setTitle(
                    "\(pickedSubjects.count) \(Text.subjectsSelected.rawValue)", for: UIControlState.normal
                )
            }
            
            if let _ = announcement, announcementWasEdited == false {
                announcementWasEdited = true
            }
        }
    }
    
    func addToArray(subject: Subject) {
        pickedSubjects.append(subject)
        print(pickedSubjects)
    }
    
    func removeFromArray(subject: Subject) {
        guard let index = pickedSubjects.index(of: subject) else {print("Index not found"); return}
        pickedSubjects.remove(at: index)
        print(pickedSubjects)
    }
    
    func discardArray() {
        pickedSubjects.removeAll()
        print(pickedSubjects)
    }
}

//MARK: - Computed Properties / IBOutlets
fileprivate extension PostAnnouncementVC {
    weak var postAnnouncementView: PostAnnouncementView! { return view as! PostAnnouncementView }
    weak var titleTextField: UITextField! { return postAnnouncementView.titleTextField }
    weak var bodyTextView: UITextView! { return postAnnouncementView.bodyTextView }
    weak var publishTextField: UITextField! { return postAnnouncementView.publishTextField }
    weak var publishPicker: UIDatePicker! { return postAnnouncementView.publishDatePicker }
    weak var expireLabel: ObservingLabel! { return postAnnouncementView.expireLabel }
    weak var expireTextField: ObservingTextField! { return postAnnouncementView.expireTextField }
    weak var expirePicker: UIDatePicker! { return postAnnouncementView.expireDatePicker }
    weak var segmentedControl: UISegmentedControl! { return postAnnouncementView.segmentedControl }
    weak var announceToButton: UIButton! { return postAnnouncementView.announceToButton }
    weak var postButton: UIButton! { return postAnnouncementView.postButton }
    weak var activityIndicator: UIActivityIndicatorView! { return postAnnouncementView.activityIndicator }
    weak var goBackButton: UIBarButtonItem! { return postAnnouncementView.goBackButton }
    weak var titleLabel: UILabel! { return postAnnouncementView.titleLabel }
    weak var postButtonItem: UIBarButtonItem! { return postAnnouncementView.postButtonItem }
}

//MARK: - Helper Functions
fileprivate extension PostAnnouncementVC {
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasDismissed(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }

    @objc func keyboardWasShown(_ notification: NSNotification)  {
        if let keyboardNSValue = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue {

            let keyboardSize = keyboardNSValue.cgRectValue.size
//            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
//            
//            UIView.animate(withDuration: 0.25) { [unowned self] in
//                self.bottomConstraint.update(inset: contentInsets.bottom + 5.0)
//                self.view.layoutIfNeeded()
//            }
            if view.frame.origin.y == 64.0 {
                UIView.animate(withDuration: 0.25) { [unowned self] in
                    self.view.frame.origin.y -= keyboardSize.height / self.keyboardHeightDivisor
                }
            }
        }
    }
    
    @objc func keyboardWasDismissed(_ notification: NSNotification)  {
//        UIView.animate(withDuration: 0.25) { [unowned self] in
//            self.bottomConstraint.update(inset: 20.0)
//            self.view.layoutIfNeeded()
//        }
        
        view.frame.origin.y = 64.0
        
    }
    
    func setUpTextFields() {
        let textFieldPickerDictionary: [UITextField: UIDatePicker] = [
            self.publishTextField: self.publishPicker, self.expireTextField: self.expirePicker
        ]

        for textField in textFields {
            textField.delegate = self
        }
        
        for (textField, picker) in textFieldPickerDictionary {
            textField.text = dateFormatter.string(from: picker.date)
        }
        
        titleTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    func setUpNavBar() {
        goBackButton.target = self
        goBackButton.action = #selector(goBackButtonPressed)
        navigationItem.leftBarButtonItem = goBackButton
        
        postButtonItem.target = self
        postButtonItem.action = #selector(postAnnouncement)
        navigationItem.rightBarButtonItem = postButtonItem
        
        if let _ = announcement {
            titleLabel.text = Text.editAnnouncement.rawValue
        }
        
        navigationItem.titleView = titleLabel
    }
    
    func presentSubjectSelectionVC() {
        guard let subjects = self.subjects else { print("No Subjects"); return }
        let subjectVC = SubjectSelectionVC(subjects: subjects)
        subjectVC.pickingDelegate = self
        self.navigationController?.pushViewController(subjectVC, animated: true)
    }
    
    func setUpAnnouncement() {
        guard let announcement = self.announcement else { return }
        titleTextField.text = announcement.title
        bodyTextView.text = announcement.body
        bodyTextView.textColor = UIColor.darkGray
        
        // Configure the date
        publishPicker.date = announcement.publishDate
        publishPicker.setDate(publishPicker.date, animated: true)
        publishTextField.text = dateFormatter.string(from: publishPicker.date)
        
        if dateFormatter.string(from: announcement.expireDate) == Text.noExpiration.rawValue {
            segmentedControl.selectedSegmentIndex = 1
            
            expireLabel.isEnabled = false
            expireTextField.isEnabled = false
            
        } else {
            expirePicker.date = announcement.expireDate
            expirePicker.setDate(expirePicker.date, animated: true)
            expireTextField.text = dateFormatter.string(from: expirePicker.date)
        }
        
        //Configure picked subjects
        if let announcementSubjects = announcement.subjects, !announcementSubjects.isEmpty {
            if let subjects = self.subjects, subjects.count == announcementSubjects.count {
                
                 pickedSubjects = [Subject(id: 0, name: Text.allSubjectName.rawValue, grade: Grade(id: 0, name: ""), topic: Topic(id: 0, name: ""))]

            } else {
            
                for subject in announcementSubjects {
                    pickedSubjects.append(subject)
                }
            }
            
            // Reset announcementWasEdited to false because the didSet property observer of pickedSubjects sets it to true
            announcementWasEdited = false
        }
        
        //Reconfigure Post button and Post button item to
        postButton.removeTarget(self, action: #selector(postAnnouncement), for: UIControlEvents.touchUpInside)
        postButton.addTarget(self, action: #selector(updateAnnouncement), for: UIControlEvents.touchUpInside)
        
        postButtonItem.action = #selector(updateAnnouncement)
    }
    
    func createParameters(queryType: QueryType) throws -> [String: Any] {
        guard let title = titleTextField.text, let bodyText = bodyTextView.text,
            let publishDate = publishTextField.text, !publishDate.isEmpty,
            let expireDate = expireTextField.text, !expireDate.isEmpty
        else {
            throw TeachatError.missingInput(ErrorText.announcementMissing.rawValue)
        }
        
        var parameters: [String: Any] = [:]
        
        guard bodyText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != ""
            else { throw TeachatError.invalidInput(ErrorText.announcementMissing.rawValue) }
        
        if segmentedControl.selectedSegmentIndex == 0, publishPicker.date > expirePicker.date {
            throw TeachatError.invalidInput(ErrorText.publishDate.rawValue)
        }
        
        let subjectIDs = pickedSubjects.map{String($0.id)}
        
        guard !subjectIDs.isEmpty
            else { throw TeachatError.missingInput(ErrorText.subject.rawValue) }
        
        parameters[JSONKey.title.rawValue] = title
        parameters[JSONKey.announcement.rawValue] = bodyText
        parameters[JSONKey.publishDate.rawValue] = publishDate
        
        switch segmentedControl.selectedSegmentIndex {
            case 0:
                parameters[JSONKey.expireDate.rawValue] = expireDate
            
            case 1:
                parameters[JSONKey.expireDate.rawValue] = Text.noExpiration.rawValue
            
            default: break
        }
        
        parameters[JSONKey.teacherSubjectID.rawValue] = subjectIDs
        parameters[JSONKey.token.rawValue] = tokenDelegate.token
        
        switch queryType {
            case .post: break
            
            case .put:
                if let announcement = announcement {
                    parameters[JSONKey.id.rawValue] = String(announcement.id)
                }
        }
        
        return parameters
    }
    
    func checkForAnnouncementEdit() {
        if let _ = announcement, announcementWasEdited == false {
            announcementWasEdited = true
        }
    }
}

//MARK: - TargetAction Functions
fileprivate extension PostAnnouncementVC {
    
    @objc func goBackButtonPressed() {
        activeTextField?.resignFirstResponder()
        activeTextView?.resignFirstResponder()
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func publishDateControlPressed() {
        publishPicker.setDate(publishPicker.date, animated: true)
        publishTextField.text = dateFormatter.string(from: publishPicker.date)
        activeTextField?.resignFirstResponder()
        
        checkForAnnouncementEdit()
    }
    
    @objc func expireDateControlPressed() {
        expirePicker.setDate(expirePicker.date, animated: true)
        expireTextField.text = dateFormatter.string(from: expirePicker.date)
        activeTextField?.resignFirstResponder()
        
        checkForAnnouncementEdit()
    }
    
    @objc func segmentedControlPressed() {
        switch segmentedControl.selectedSegmentIndex {
            case 0: // Yes
                expireLabel.isEnabled = true
                expireTextField.isEnabled = true
        
            case 1: // No
                expireLabel.isEnabled = false
                expireTextField.isEnabled = false
            
            default: break
        }
        
        checkForAnnouncementEdit()
    }
    
    @objc func announceToButtonPressed() {
        switch pickedSubjects.isEmpty {
            case true:
                presentSubjectSelectionVC()
            case false:
                present(alertActionSheet, animated: true, completion: nil)
        }
    }
    
    @objc func postAnnouncement() {
        do {
            let parameters = try createParameters(queryType: QueryType.post)
            switch parameters.isEmpty {
                
                case false:
                
                    apiDelegate.query(endpoint: APIManager.Endpoint.announcementPost, parameters: parameters) {
                        [unowned self] (result: Result) in
                        DispatchQueue.main.async {
                        
                            switch result {
                                case .Success:
                                    self.tableDelegate.reloadOnAppear = true
                                    self.goBackButtonPressed()
                            
                                case .Error(let error):
                                    guard let error = error as? NetworkingError
                                        else { print("Error at \(self) line 473"); return }
                                    self.present(alert: self.alert, error: error)
                            }
                        }
                    }
                case true:
                    let error = TeachatError.missingInput(ErrorText.announcementMissing.rawValue)
                    
                    self.alert.title = error.title
                    self.alert.message = error.localizedDescription
                    
                    self.present(alert: self.alert)
            }
        } catch let error {
            
            guard let error = error as? DisplayableError
                else { print("Error type not handled at \(self) line 500"); return }
            
            self.alert.title = error.title
            self.alert.message = error.localizedDescription
            
            self.present(alert: self.alert)
        }
    }
    
    func queryForSubjects() {
        activityIndicator.startAnimating()
        view.isUserInteractionEnabled = false
        
        let parameters = [JSONKey.token.rawValue : tokenDelegate.token]
        apiDelegate.query(endpoint: APIManager.Endpoint.subjectGet, parameters: parameters) {
            [unowned self] (result: Result) in
            DispatchQueue.main.async {
                
                switch result {
                    case .Success(let data):
                        do {
                            self.subjects = try self.subjectDelegate.createSubjects(with: data)
                            self.setUpAnnouncement()
                        } catch let error {
                            guard let error = error as? DisplayableError
                                else { print("Error type not handled at \(self) line 508"); return }
                        
                            self.alert.title = error.title
                            self.alert.message = error.localizedDescription
                        
                            self.present(alert: self.alert)
                        }
                
                    case .Error(let error):
                
                        guard let error = error as? NetworkingError else { print("Error at \(self) line 518"); return }
                        self.present(alert: self.alert, error: error)
                }
                
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                
            }
        }
    }
    
    @objc func updateAnnouncement() {
        
        do {
            let parameters = try createParameters(queryType: QueryType.put)
           
            switch parameters.isEmpty {
                
                case true:
                
                    let error = TeachatError.missingInput(ErrorText.announcementMissing.rawValue)
                
                    self.alert.title = error.title
                    self.alert.message = error.localizedDescription
                
                    self.present(alert: self.alert)
                
                case false:
                
                    apiDelegate.query(endpoint: APIManager.Endpoint.announcementUpdate, parameters: parameters) {
                        [unowned self] (result: Result) in
                    
                        DispatchQueue.main.async {
                        
                            switch result {
                            
                                case .Success:
                                    self.tableDelegate.reloadOnAppear = true
                                    self.goBackButtonPressed()
                            
                            
                                case .Error(let error):
                                    guard let error = error as? NetworkingError
                                        else { print("Error at \(self) line 469"); return }
                                    self.present(alert: self.alert, error: error)
                            }
                        }
                    }
            }
            
        } catch let error as NSError {
            guard let error = error as? DisplayableError
                else { print("Error type not handled at \(self) line 500"); return }
            
            self.alert.title = error.title
            self.alert.message = error.localizedDescription
            
            self.present(alert: self.alert)
        }
    }
}

// MARK: - UITextFieldDelegate Functions
extension PostAnnouncementVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        activeTextField = nil
        
    }
    
    // MARK: Custom textField Function
    func textFieldDidChange(_ textField: UITextField) {
        guard let titleIsEmpty = titleTextField.text?.isEmpty else { return }
        
        let enable = !titleIsEmpty && bodyTextView.text != postAnnouncementView.placeholderText
        
        postButton.isEnabled = enable
        postButtonItem.isEnabled = enable
    }
}

// MARK: - UITextViewDelegate Functions
extension PostAnnouncementVC: UITextViewDelegate {
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.text == postAnnouncementView.placeholderText, textView.textColor == TeachatUI.Color.placeHolderGray {
            textView.selectedRange = NSMakeRange(0, 0)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        activeTextView = textView
        textView.selectedRange = NSMakeRange(0, 0)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == postAnnouncementView.placeholderText, textView.textColor == TeachatUI.Color.placeHolderGray {
            textView.text = textView.text.substring(to: textView.text.startIndex)
            textView.textColor = UIColor.darkGray
        } else if textView.text.isEmpty {
            textView.text = postAnnouncementView.placeholderText
            textView.textColor = TeachatUI.Color.placeHolderGray
            textView.selectedRange = NSMakeRange(0, 0)
        }
        
        guard let titleIsEmpty = titleTextField.text?.isEmpty else { return }
        let enable = !titleIsEmpty && bodyTextView.text != postAnnouncementView.placeholderText
        postButton.isEnabled = enable
        postButtonItem.isEnabled = enable
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        activeTextView = nil
        
        switch textView.text.isEmpty {
            case true:
                textView.text = postAnnouncementView.placeholderText
                textView.textColor = TeachatUI.Color.placeHolderGray
            case false:
                textView.text = textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text == postAnnouncementView.placeholderText {
            textView.text.removeAll()
            textView.textColor = UIColor.darkGray
        }
        
        return true
    }
}
