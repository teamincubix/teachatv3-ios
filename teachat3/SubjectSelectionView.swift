//
//  SubjectSelectionView.swift
//  teachat3
//
//  Created by Julio Alorro on 10/12/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

class SubjectSelectionView: UIView {
    // MARK: Text
    private enum Text: String {
        case subjectSelection = "Select Subject(s) To Announce To"
        case add = "Add Subject(s)"
        case cancel = "Cancel"
        case navBarTitle = "Subject Selection"
    }
    
    // MARK: - Subviews
    private let subjectSelectionLabel: UILabel = {
        let label = UILabel()
        label.text = Text.subjectSelection.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 14.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.white
        return label
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = true
        tableView.backgroundColor = UIColor.white
        return tableView
    }()
    
    var tableViewHeightConstraint: Constraint!
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.cancelButton, self.addButton])
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 10.0
        return stackView
    }()
    
    let addButton: ObservingButton = {
        let button = ObservingButton(type: UIButtonType.system)
        button.setTitle(Text.add.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.teal
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        button.isEnabled = false
        return button
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle(Text.cancel.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.red
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()
    
    // MARK: - Navigation Items
    let cancelButtonItem: UIBarButtonItem = {
        let button = UIBarButtonItem(
            title: Text.cancel.rawValue, style: UIBarButtonItemStyle.done, target: nil, action: nil
        )
        button.setTitleTextAttributes(TeachatUI.NavBar.attributes, for: UIControlState.normal)
        return button
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 44.0, height: 44.0))
        label.text = Text.navBarTitle.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.bold.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: - Stored Properties
    private lazy var subviewsArray: [UIView] = [self.subjectSelectionLabel, self.tableView, self.stackView]
    
    // MARK: - UIView Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        setSubviewsForAutoLayout(subviewsArray)
        
        // MARK: Constraints
        subjectSelectionLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            make.height.equalTo(self).multipliedBy(0.075)
        }
        
        tableView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.subjectSelectionLabel.snp.bottom)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            self.tableViewHeightConstraint = make.height.equalTo(0).constraint
        }
        
        stackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.bottom.equalTo(self).offset(-20.0)
            make.height.equalTo(self).multipliedBy(0.075)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        subjectSelectionLabel.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor.lightGray, thickness: 0.5)
    }
}
