//
//  LoginView.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class LoginView: UIView {
    private enum Text: String {
        case videoText = "Video Chat with Teachat"
        case parentText = "Parent-Teacher Conference made simple"
        case email = "Email Address"
        case password = "Password"
        case login = "Log In"
        case or = "OR"
        case facebook = "Connect with Facebook"
        case forgotPassword = "Forgot Password?"
        case register = "Don't have an account yet? Sign up"
    }
    
    fileprivate let background: UIImageView = {
        let imageView = UIImageView()
        imageView.image = TeachatUI.Image.background
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        return imageView
    }()
    
    let logo: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.image = TeachatUI.Image.teachat
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        return imageView
    }()
    
    var bottomLogoConstraint: Constraint!
    
    fileprivate lazy var labelStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.videoChatLabel, self.parentLabel])
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillProportionally
        stackView.spacing = 0.0
        return stackView
    }()
    
    fileprivate let videoChatLabel: UILabel = {
        let label = UILabel()
        label.text = Text.videoText.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 30.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    fileprivate let parentLabel: UILabel = {
        let label = UILabel()
        label.text = Text.parentText.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 24.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    lazy var loginStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.errorLabel, self.emailTextField, self.passwordTextField, self.loginButton])
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fill
        stackView.spacing = 10.0
        return stackView
    }()
    
    let errorLabel: TeachatLabel = {
        let label = TeachatLabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 13.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.textColor = UIColor.red
        label.backgroundColor = TeachatUI.Color.warningRed
        label.textAlignment = NSTextAlignment.center
        label.alpha = 0.0
        label.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return label
    }()
    
    let emailTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.email.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.setUpKeyboard(UITextField.KeyboardType.email, returnKeyType: UIReturnKeyType.next)
        
        let imageView = UIImageView(image: TeachatUI.Image.email)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let passwordTextField: TeachatTextField = {
        let textField = TeachatTextField()
        textField.placeholder = Text.password.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.setUpKeyboard(UITextField.KeyboardType.password, returnKeyType: UIReturnKeyType.send)
        
        let imageView = UIImageView(image: TeachatUI.Image.password)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = UITextFieldViewMode.always
        
        return textField
    }()
    
    let loginButton: TeachatButton = {
        let button = TeachatButton(type: UIButtonType.custom)
        button.setTitle(Text.login.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.green
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()

    fileprivate let orLabel: UILabel = {
        let label = UILabel()
        label.text = Text.or.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    fileprivate lazy var buttonStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.facebookButton, self.forgotPasswordButton, self.registerButton])
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 5.0
        return stackView
    }()
    
    let facebookButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle(Text.facebook.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = TeachatUI.Color.facebookBlue
        button.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return button
    }()
    
    let forgotPasswordButton: TeachatButton = {
        let button = TeachatButton(type: UIButtonType.custom)
        button.setTitle(Text.forgotPassword.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    let registerButton: TeachatButton = {
        let button = TeachatButton(type: UIButtonType.custom)
        button.setTitle(Text.register.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.button)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.backgroundColor = TeachatUI.Color.lightGray
        indicator.color = TeachatUI.Color.green
        indicator.hidesWhenStopped = true
        indicator.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return indicator
    }()
    
    private var subviewsArray: [UIView] {
        return [
            self.background, self.logo, self.labelStackView, self.loginStackView,
            self.orLabel, self.buttonStackView, self.activityIndicator
        ]
    }
        
    fileprivate var textFieldIconDictionary: [UIView: UIImage?] {
        return [
            self.emailTextField: TeachatUI.Image.email, self.passwordTextField: TeachatUI.Image.password
        ]
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setSubviewsForAutoLayout(subviewsArray)
        
        background.snp.remakeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(self)
        }
        
        buttonStackView.snp.remakeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(-15.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
        }
        
        orLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(20.0)
            make.bottom.equalTo(self.buttonStackView.snp.top).offset(-15.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
        }
        
        loginStackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.bottom.equalTo(self.orLabel.snp.top).offset(-15.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.centerX.equalTo(self)
        }
        
        errorLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.height.equalTo(self).multipliedBy(0.0)
        }
        
        for view in [emailTextField, passwordTextField, loginButton, facebookButton] as [UIView] {
            
            view.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
                make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
            
            }
        }
        
        labelStackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.bottom.equalTo(self.loginStackView.snp.top).offset(0.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
        }
        
        logo.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.height.equalTo(self.snp.height).multipliedBy(0.10)
            self.bottomLogoConstraint = make.bottom.equalTo(self.labelStackView.snp.top).offset(-15.0).constraint
            make.width.equalTo(self.logo.snp.height).multipliedBy(4.5)
        }
        
        activityIndicator.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.width.equalTo(self.activityIndicator.snp.height)
            make.height.equalTo(self).multipliedBy(0.10)
            make.center.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        for textField in [self.emailTextField, self.passwordTextField] {
            
            guard let leftView = textField.leftView else { continue }
            
            leftView.frame.size = CGSize(width: textField.iconWidth, height: textField.iconHeight)
            
        }
        
        facebookButton.createSubImage(with: TeachatUI.Image.facebook, padding: TeachatUI.Icon.padding, alpha: 1.0)
        
    }
}
