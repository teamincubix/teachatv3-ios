//
//  ArrayDataSource.swift
//  teachat3
//
//  Created by Julio Alorro on 11/14/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

class AnnouncementDataSource<Type, Cell: UITableViewCell>: DataSource<ArrayProvider<Type>, Cell> where Cell: ConfigurableCell, Cell.Object == Type {
    
    // MARK: - Stored Properties
    var objects: [Type] { return provider.objects }
    fileprivate var viewController: AnnouncementVC
    
    // MARK: - Initializer
    init(tableView: UITableView, array: [Type], viewController: AnnouncementVC) {
        let provider = ArrayProvider(objects: array)
        self.viewController = viewController
        super.init(provider: provider, tableView: tableView)
    }
    
    // MARK: - Deinitialization
    deinit {
        print("\(self) was deallocated")
    }
    
    // MARK: - Instance Methods
    override func setup() {
        super.setup()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250.0
        tableView.allowsSelection = false
    }
    
    // MARK: - UITableViewDataSource Methods
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! AnnouncementCell
        cell.cacheDelegate = self.viewController.dataDelegate
        cell.imageDelegate = self.viewController.apiDelegate
        cell.announcementEditingDelegate = self.viewController
        cell.userID = self.viewController.userID
        let announcement = provider.object(at: indexPath) as! Announcement
        cell.configure(with: announcement, at: indexPath)
        return cell
    }
}
