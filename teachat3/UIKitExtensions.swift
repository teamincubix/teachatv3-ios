//
//  UIKitExtensions.swift
//  teachat3
//
//  Created by Julio Alorro on 9/16/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit

// MARK: - UIView Extension
extension UIView {
    internal func createSubImage(with image: UIImage?, padding: CGFloat, alpha: CGFloat) {
        guard let image = image else { return }
        let imageView: UIImageView = UIImageView(image: image)
        let pad: CGFloat = padding * 2.0
        let imageSize = CGSize(width: self.bounds.size.height - pad, height: self.bounds.size.height - pad)
        imageView.frame = CGRect(x: padding, y: padding, width: imageSize.width, height: imageSize.height)
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.alpha = alpha
        imageView.accessibilityIdentifier = "Sub Icon"
        self.addSubview(imageView)
    }
    
    internal func deleteSubImage() {
        for subview in self.subviews {
            if let imageView = subview as? UIImageView, imageView.accessibilityIdentifier == "Sub Icon" {
                imageView.removeFromSuperview()
            }
        }
    }
    
    /**
     Adds the UIView as a subview and sets that subview's translatesAutoresizingMaskIntoConstraints property to false
     
     - parameter subview: the UIView that becomes a subview
     */
    internal func setSubviewForAutoLayout(_ subview: UIView) {
        self.addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
    }
    
    /**
     Adds the array of UIViews as subviews using the setUpSubView function
     
     - paramater subviews: the UIView array to be added to the view hierarchy
     */
    internal func setSubviewsForAutoLayout(_ subviews: [UIView]) {
        _ = subviews.map{self.setSubviewForAutoLayout($0)}
    }
    
    /**
     Sets the calling view's cornerRadius as well as sets the clipsToBounds property to true
     
     - parameter radius: the value for the cornerRadius
     
     */
    internal func setCornerRadius(to cornerRadius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
    }
    
}

extension UITextField {
    
    var iconWidth: CGFloat {
        return self.bounds.width / 7.0
    }
    
    var iconHeight: CGFloat {
        return self.bounds.height * 0.6
    }
    
}

extension UISegmentedControl {
    
    internal func setFont(to font: UIFont?) {
        guard let font = font else { print("Font did not initialize"); return }
        let textAttributes = [NSFontAttributeName: font]
        self.setTitleTextAttributes(textAttributes, for: UIControlState.normal)
    }
}

// MARK: - UITextField Extension
extension UITextField {
    internal enum KeyboardType {
        case email, password, numbers, phonenumber, normal
    }
    
    internal func setUpKeyboard(_ type: KeyboardType, returnKeyType: UIReturnKeyType) {
        self.autocapitalizationType = UITextAutocapitalizationType.none
        self.autocorrectionType = UITextAutocorrectionType.no
        self.spellCheckingType = UITextSpellCheckingType.no
        self.keyboardAppearance = UIKeyboardAppearance.default
        self.returnKeyType = returnKeyType
        self.clearButtonMode = UITextFieldViewMode.always
        
        switch type {
            case .email:
                self.keyboardType = UIKeyboardType.emailAddress
            
            case .password:
                self.keyboardType = UIKeyboardType.default
                self.isSecureTextEntry = true
            
            case .numbers:
                self.keyboardType = UIKeyboardType.numbersAndPunctuation
            
            case . phonenumber:
                self.keyboardType = UIKeyboardType.phonePad
            
            case .normal:
                self.keyboardType = UIKeyboardType.default
        }
    }
}

// MARK: - UIColor Extension
extension UIColor {
    internal convenience init(red: UInt8, green: UInt8, blue: UInt8) {
        let red: CGFloat = CGFloat(red) / 255.0, green: CGFloat = CGFloat(green) / 255.0, blue: CGFloat = CGFloat(blue) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    internal convenience init(netHex: UInt8) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

// MARK: - UITabBarController Extension
extension UITabBarController {
    internal func setUpTeachatUI() {
        self.tabBar.barTintColor = TeachatUI.Color.lightGray
        self.tabBar.tintColor = TeachatUI.Color.green
    }
}

// MARK: - UINavigationController Extension
extension UINavigationController {
    internal func makeLogo(with image: UIImage?) {
        guard let image = image else { return }
        let imageView: UIImageView = UIImageView(image: image)
        let imageSize = CGSize(width: self.navigationBar.bounds.width * 0.4, height: self.navigationBar.bounds.height * 0.5)
        imageView.bounds = CGRect(x: 0.0, y: 0.0, width: imageSize.width, height: imageSize.height)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.center = CGPoint(x: navigationBar.bounds.midX, y: navigationBar.bounds.midY)
        imageView.accessibilityIdentifier = TeachatUI.NavBar.logoName
        imageView.backgroundColor = .black
        self.navigationBar.addSubview(imageView)
    
    }
    
    internal func setUpNavUI() {
        self.navigationBar.barTintColor = TeachatUI.Color.navBar
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.isTranslucent = false
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }
}

// MARK: - UIAlertController Extension
extension UIAlertController {
    
    var defaultDismissAction: UIAlertAction {
        return UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel) { (_: UIAlertAction) in
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
            case UIRectEdge.top:
                
                border.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.height, height: thickness)
            
            case UIRectEdge.bottom:
                
                border.frame = CGRect(x: 0.0, y: self.bounds.height - thickness, width: self.bounds.width, height:  thickness)
            
            case UIRectEdge.left:
                
                border.frame = CGRect(x: 0.0, y: 0.0, width: thickness, height: self.bounds.height)
            
            case UIRectEdge.right:
                
                border.frame = CGRect(x: self.bounds.width - thickness, y: 0.0, width: thickness, height: self.bounds.height)
                
            default: break
        }
        
        border.backgroundColor = color.cgColor
        
        self.addSublayer(border)
    }
    
}
