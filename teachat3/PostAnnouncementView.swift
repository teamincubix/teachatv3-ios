//
//  PostAnnouncementView.swift
//  teachat3
//
//  Created by Julio Alorro on 10/7/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class PostAnnouncementView: UIView {
    
    // MARK: Text
    private enum Text: String {
        case subject = "Which subject to announce to"
        case title = "Announcement Title"
        case announcement = "Write your announcement here..."
        case publishDate = "Publish date"
        case expirationDate = "Expiration date"
        case doesExpire = "Set to expire"
        case yes = "Yes"
        case no = "No"
        case announceTo = "Select Subjects"
        case post = "Post"
        case date = "MMM dd, yyyy"
        case goBack = "Go Back"
        case navBarTitle = "Post Announcement"
    }
    
    // MARK: - Subviews
    let titleTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = Text.title.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.none
        textField.returnKeyType = UIReturnKeyType.next
        return textField
    }()
    
    let bodyTextView: UITextView = {
        let textView = UITextView(frame: CGRect.zero)
        textView.text = Text.announcement.rawValue
        textView.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        textView.textColor = TeachatUI.Color.placeHolderGray
        textView.isEditable = true
        textView.textAlignment = NSTextAlignment.left
        textView.returnKeyType = UIReturnKeyType.send
        return textView
    }()
    
    fileprivate lazy var stackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.publishStackView, self.expireStackView/*, self.announceToButton, self.postButton*/])
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 10.0
        return stackView
        }()
    
    fileprivate lazy var publishStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.publishLabel, self.publishTextField])
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 0.0
        return stackView
    }()
    
    fileprivate let publishLabel: UILabel = {
        let label = UILabel()
        label.text = Text.publishDate.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.white
        return label
    }()
    
    lazy var publishTextField: UITextField = { [unowned self] in
        let textField = UITextField()
        textField.placeholder = Text.date.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.none
        textField.inputView = self.publishDatePicker
        return textField
    }()
    
    fileprivate lazy var expireStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.expireLabel, self.expireTextField])
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 0.0
        return stackView
    }()
    
    let expireLabel: ObservingLabel = {
        let label = ObservingLabel()
        label.text = Text.expirationDate.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.white
        return label
    }()
    
    lazy var expireTextField: ObservingTextField = { [unowned self] in
        let textField = ObservingTextField()
        textField.placeholder = Text.date.rawValue
        textField.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        textField.textColor = UIColor.darkGray
        textField.minimumFontSize = TeachatUI.FontSize.textField
        textField.borderStyle = UITextBorderStyle.none
        textField.inputView = self.expireDatePicker
        return textField
    }()
    
    lazy var publishDatePicker: UIDatePicker = { [unowned self] in
        let picker = UIDatePicker()
        picker.datePickerMode = UIDatePickerMode.date
        picker.timeZone = TimeZone.current
        picker.minimumDate = self.date
        return picker
    }()
    
    lazy var expireDatePicker: UIDatePicker = { [unowned self] in
        let picker = UIDatePicker()
        picker.datePickerMode = UIDatePickerMode.date
        picker.timeZone = TimeZone.current
        picker.minimumDate = self.date
        return picker
    }()
    
    fileprivate lazy var segmentedControlStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.doesExpireLabel, self.segmentedControl])
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.spacing = 10.0
        return stackView
    }()
    
    fileprivate let doesExpireLabel: UILabel = {
        let label = UILabel()
        label.text = Text.doesExpire.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.white
        return label
    }()
    
    let segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl(items: [Text.yes.rawValue, Text.no.rawValue])
        control.tintColor = TeachatUI.Color.green
        control.backgroundColor = UIColor.white
        control.selectedSegmentIndex = 0
        control.setFont(to: UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0))
        control.layer.borderColor = TeachatUI.Color.green.cgColor
        control.layer.borderWidth = 1.0
        return control
    }()
    
    fileprivate lazy var buttonStackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.announceToButton, self.postButton])
        stackView.axis = UILayoutConstraintAxis.horizontal
        stackView.alignment = UIStackViewAlignment.fill
        stackView.distribution = UIStackViewDistribution.fill
        stackView.spacing = 10.0
        return stackView
    }()
    
    let announceToButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle(Text.announceTo.rawValue, for: UIControlState.normal)
        button.setTitleColor(TeachatUI.Color.green, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = TeachatUI.Color.green.cgColor
        button.layer.borderWidth = 1.0
        return button
    }()
    
    let postButton: ObservingButton = {
        let button = ObservingButton(type: UIButtonType.system)
        button.setTitle(Text.post.rawValue, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 15.0)
        button.backgroundColor = TeachatUI.Color.teal
        button.isEnabled = false
        return button
    }()
    
    let goBackButton: UIBarButtonItem = {
        let button = UIBarButtonItem(
            title: Text.goBack.rawValue, style: UIBarButtonItemStyle.plain, target: nil, action: nil
        )
        
        button.setTitleTextAttributes(TeachatUI.NavBar.attributes, for: UIControlState.normal)
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.backgroundColor = TeachatUI.Color.lightGray
        indicator.color = TeachatUI.Color.green
        indicator.hidesWhenStopped = true
        indicator.setCornerRadius(to: TeachatUI.View.cornerRadius)
        return indicator
    }()
    
    // MARK: - Navigation Items
    let titleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 44.0, height: 44.0))
        label.text = Text.navBarTitle.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.bold.rawValue, size: 15.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        return label
    }()
    
    
    
    let postButtonItem: UIBarButtonItem = {
        let button = UIBarButtonItem(
            image: TeachatUI.Image.post, style: UIBarButtonItemStyle.plain, target: nil, action: nil
        )
        button.isEnabled = false
        return button
    }()
    
    // MARK: - Stored Properties
    let placeholderText = Text.announcement.rawValue
    let announceToButtonText = Text.announceTo.rawValue
    
    // UIDatePickers must share the same date object to be comparable accurately
    private let date = Date()
    private lazy var subviewsArray: [UIView] = [
        self.titleTextField, self.bodyTextView, self.stackView, self.segmentedControlStackView,
        self.buttonStackView, self.activityIndicator
    ]
    
    fileprivate lazy var textFields: [UIView] = [self.titleTextField, self.publishTextField, self.expireTextField]
    fileprivate lazy var buttons: [UIButton] = [self.announceToButton, self.postButton]
    
    // MARK: - UIView Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setSubviewsForAutoLayout(subviewsArray)
        backgroundColor = UIColor.white
        
        // MARK: Constraints
        titleTextField.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self).offset(20.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
        }
        
        bodyTextView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.titleTextField.snp.bottom).offset(8.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.bottom.equalTo(self.stackView.snp.top).offset(-8.0)
        }
        
        stackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.height.equalTo(self).multipliedBy(0.15)
        }
        
        segmentedControlStackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.stackView.snp.bottom).offset(20.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
        }
        
        buttonStackView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.segmentedControlStackView.snp.bottom).offset(20.0)
            make.leading.equalTo(self).offset(20.0)
            make.trailing.equalTo(self).offset(-20.0)
            make.bottom.equalTo(self).offset(-20)
        }
        
        postButton.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.width.equalTo(self.announceToButton).multipliedBy(0.50)
            make.height.equalTo(self).multipliedBy(TeachatUI.View.ratio)
        }
        
        activityIndicator.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.width.equalTo(self.activityIndicator.snp.height)
            make.height.equalTo(self).multipliedBy(0.10)
            make.center.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        for textField in textFields {
            textField.layer.addBorder(edge: UIRectEdge.bottom, color: TeachatUI.Color.lightGray, thickness: 0.75)
        }
        
        for button in buttons {
            button.setCornerRadius(to: button.frame.height / 2)
        }
        
        segmentedControl.setCornerRadius(to: segmentedControl.frame.height / 2)
        
    }
}
