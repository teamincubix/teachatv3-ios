//
//  SubjectSelectionVC.swift
//  teachat3
//
//  Created by Julio Alorro on 10/12/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

class SubjectSelectionVC: UIViewController, TargetActionSetting {
    
    // MARK: - Delegate Properties
    weak var pickingDelegate: SubjectPicking?
    
    // MARK: - Custome Initializer
    convenience init(subjects: [Subject]) {
        self.init(nibName: nil, bundle: nil)
        self.subjects.append(contentsOf: subjects)
        self.numberOfSubjects = subjects.count
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Controller Lifecycle Methods
    override func loadView() {
        view = SubjectSelectionView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        setUpNavBar()
        setUpTargetActions(targetActionDictionary)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let subjects = pickingDelegate?.pickedSubjects, !subjects.isEmpty {
            addButton.isEnabled = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // TODO: deactivate and constrain to bottom after a certain number of subjects
        tableViewHeightConstraint.update(offset: tableView.contentSize.height)
        tableView.layoutIfNeeded()
    }
    
    // MARK: - Deinitilization
    deinit {
        print("\(self) was deallocated")
    }
    
    // MARK: - Text Enum
    fileprivate enum Text: String {
        case discardTitle = "Discard selected subjects?"
        case discard = "Discard"
        case goBack = "Go Back"
        case subjectCell = "SubjectCell"
        case allSubjects = "All Subjects"
    }
    
    //MARK: - Stored Properties
    fileprivate let defaultSubject: Subject = Subject(id: 0, name: Text.allSubjects.rawValue, grade: Grade(id: 0, name: ""), topic: Topic(id: 0, name: ""))
    
    fileprivate lazy var subjects: [Subject] = [self.defaultSubject]
    
    // Subjects added by API
    fileprivate var numberOfSubjects: Int = 0
    fileprivate var subjectsSelectedCounter: Int = 0 {
        didSet {
            if subjectsSelectedCounter == numberOfSubjects {
                allSubjectsSelected()
            }
        }
    }
    
    fileprivate var selectionWasEdited: Bool = false
    
    // MARK: Alert Controller Action Sheet
    fileprivate lazy var alert: UIAlertController = { [unowned self] in
        let alert = UIAlertController(title: Text.discardTitle.rawValue, message: nil , preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actions: [UIAlertAction] = [
            UIAlertAction(title: Text.discard.rawValue, style: UIAlertActionStyle.destructive) {
                [unowned self] (_: UIAlertAction) in
                self.pickingDelegate?.discardArray()
                
                _ = self.navigationController?.popViewController(animated: true)
            },
            UIAlertAction(title: Text.goBack.rawValue, style: UIAlertActionStyle.cancel) {
                [unowned self] (_: UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
        ]
        
        for action in actions {
            alert.addAction(action)
        }
        
        return alert
    }()
    
    fileprivate lazy var targetActionDictionary: [UIControl: Selector] = [
        self.cancelButton: #selector(cancelButtonPressed), self.addButton: #selector(addButtonPressed)
    ]
}

// MARK: - Computed Properties / IBOutlets
fileprivate extension SubjectSelectionVC {
    weak var subjectSelectionView: SubjectSelectionView! { return view as! SubjectSelectionView }
    weak var tableView: UITableView! { return subjectSelectionView.tableView }
    weak var tableViewHeightConstraint: Constraint! { return subjectSelectionView.tableViewHeightConstraint }
    weak var addButton: ObservingButton! { return subjectSelectionView.addButton }
    weak var cancelButton: UIButton! { return subjectSelectionView.cancelButton }
    weak var cancelButtonItem: UIBarButtonItem! { return subjectSelectionView.cancelButtonItem }
    weak var titleLabel: UILabel! { return subjectSelectionView.titleLabel }
}

// MARK: - Helper Functions
fileprivate extension SubjectSelectionVC {
    
    func setUpTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SubjectCell.self, forCellReuseIdentifier: Text.subjectCell.rawValue)
    }
    
    func setUpNavBar() {
        cancelButtonItem.target = self
        cancelButtonItem.action = #selector(cancelButtonPressed)
        
        navigationItem.leftBarButtonItem = cancelButtonItem
        navigationItem.titleView = titleLabel
    }
    
    func allSubjectsSelected() {
        if let subjects = pickingDelegate?.pickedSubjects, !subjects.isEmpty {
            pickingDelegate?.discardArray()
        }
        
        pickingDelegate?.addToArray(subject: defaultSubject)
        tableView.reloadData()
        
        self.addButtonPressed()
    }
}

// MARK: - Target Action Functions
fileprivate extension SubjectSelectionVC {
    @objc func cancelButtonPressed() {
        if let subjects = pickingDelegate?.pickedSubjects, !subjects.isEmpty, selectionWasEdited {
            
            present(alert, animated: true, completion: nil)

        } else {
        
            _ = navigationController?.popViewController(animated: true)
        
        }
    }
    
    @objc func addButtonPressed() {
        
        if let subjects = pickingDelegate?.pickedSubjects, !subjects.isEmpty {
            
            _ = navigationController?.popViewController(animated: true)
            
        }
    }
}

// MARK: - UITableViewDelegate Functions
extension SubjectSelectionVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? SubjectCell else { print("Cant display subject"); return }
        if cell.accessoryType == .checkmark, cell.subject.id != 0 {
            subjectsSelectedCounter += 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? SubjectCell else { print("No Subject Cells"); return }
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if cell.subject.id == 0 {
            allSubjectsSelected()
        }
        
        switch cell.accessoryType {
            case .none:
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                
                if let subjects = pickingDelegate?.pickedSubjects, subjects.index(of: cell.subject) == nil {
                    pickingDelegate?.addToArray(subject: cell.subject)
                    addButton.isEnabled = true
                    
                    if cell.subject.id != 0 {
                        subjectsSelectedCounter += 1
                        selectionWasEdited = true
                    }
                }
            
            case .checkmark:
                cell.accessoryType = UITableViewCellAccessoryType.none
                pickingDelegate?.removeFromArray(subject: cell.subject)
                if let subjects = pickingDelegate?.pickedSubjects, subjects.isEmpty {
                    addButton.isEnabled = false
                }
            
                if cell.subject.id != 0 {
                    subjectsSelectedCounter -= 1
                    selectionWasEdited = true
                }
            
            
            
            default: break
            
        }
    }
}

// MARK: - UITableViewDataSource Functions
extension SubjectSelectionVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Text.subjectCell.rawValue) as! SubjectCell
        
        cell.configure(with: subjects[indexPath.row])
        
        if let subjects = pickingDelegate?.pickedSubjects, !subjects.isEmpty {
        
            cell.find(in: subjects)
            
        }
        
        if subjectsSelectedCounter == numberOfSubjects, cell.subject.id != 0 {
            
            cell.accessoryType = UITableViewCellAccessoryType.none
            
        } else if subjectsSelectedCounter == numberOfSubjects, cell.subject.id == 0 {
            
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        
        }
        
        return cell
    }
}

