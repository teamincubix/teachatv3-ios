//
//  AnnouncementsVC.swift
//  teachat3
//
//  Created by Julio Alorro on 9/20/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

final class AnnouncementVC: UIViewController, TableReloading, TokenSharing, AlertHandling, TargetActionSetting {
    
    // MARK: - Delegate Properties
    let apiDelegate: APIQuerying & ImageFetching
    let dataDelegate: DataGetting & DataRemoving & DataCaching
    var announcementDelegate: AnnouncementCreating
    
    // MARK: - Custom Initializer
    init(apiDelegate: APIQuerying & ImageFetching, dataDelegate: DataGetting & DataRemoving & DataCaching, announcementDelegate: AnnouncementCreating) {
        self.apiDelegate = apiDelegate
        self.dataDelegate = dataDelegate
        self.announcementDelegate = announcementDelegate
        super.init(nibName: nil, bundle: nil)
        
        tabBarItem.image = TeachatUI.Image.announcement
        
        guard let dictionary = self.dataDelegate.getData() else {
            print("User dict not found"); return
        }
        guard let user = User(dictionary: dictionary) else {
            print("Could not make user struct"); return
        }
        
        self.user = user
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Controller Lifecycle Methods
    override func loadView() {
        view = AnnouncementView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAnnouncements()
        dataSource = AnnouncementDataSource(tableView: self.tableView, array: [Announcement](), viewController: self)
        setUpTargetActions([
            self.refreshControl: #selector(refresh)
        ])
        setUpNavigationBar(role: self.user.role)
        
//        _ = dataDelegate.removeData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        
        if reloadOnAppear {
            getAnnouncements()
            reloadOnAppear = false
        }
    }
    
    // MARK: - Deinitilization
    deinit {
        print("\(self) was deallocated")
    }
    
    // MARK: - Stored Properties
    fileprivate var user: User!
    
    fileprivate var hasNoAnnouncements: Bool = true {
        didSet {
            switch hasNoAnnouncements {
                case true:
                    noAnnouncementLabel.isHidden = false
                    tableView.isHidden = true
                
                case false:
                    UIView.transition(with: self.noAnnouncementLabel, duration: 0.3, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { [unowned self] in
                        self.noAnnouncementLabel.isHidden = true
                        self.tableView.isHidden = false
                        }, completion: nil
                    )
            }
        }
    }
    
    //MARK: -  TableReloading Protocol Stored Property
    // See viewWillAppear method
    var reloadOnAppear = false
    
    // MARK: - Token Sharing Protocol Stored Property
    var token: String! {
        return self.user.token
    }

    fileprivate lazy var alert: UIAlertController = {
        return UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    }()
    
    var dataSource: AnnouncementDataSource<Announcement, AnnouncementCell>!
    
    // MARK: - User ID Computed Property for AnnouncementDataSource 
    var userID: Int! {
        return user.id
    }
}

// MARK: - Computed Properties / IBOutlets
extension AnnouncementVC {
    fileprivate weak var announcementView: AnnouncementView! { return view as! AnnouncementView }
    fileprivate weak var refreshControl: UIRefreshControl! { return announcementView.refreshControl }
    fileprivate weak var tableView: UITableView! { return announcementView.tableView }
    weak var noAnnouncementLabel: UILabel! { return announcementView.noAnnouncementLabel }
    weak var postButtonItem: UIBarButtonItem! { return announcementView.buttonItem }
    fileprivate weak var titleView: UIImageView! { return announcementView.titleView }
}

// MARK: - Helper Functions
fileprivate extension AnnouncementVC {
    func setUpNavigationBar(role: User.Role) {
        navigationItem.titleView = titleView
        
        switch role {
            case .teacher:
                
                // Filler so titleView is appropriately centered
                navigationItem.leftBarButtonItem = UIBarButtonItem(image: TeachatUI.Image.post, style: UIBarButtonItemStyle.plain, target: nil, action: nil)
                
                postButtonItem.target = self
                postButtonItem.action = #selector(postAnnouncementButtonPressed)
                navigationItem.rightBarButtonItem = postButtonItem
        
            case .parent: break
            
        }
    }
}

// MARK: - APIQuerying Function
extension AnnouncementVC {
    
    func getAnnouncements() {
        let parameters = [JSONKey.token.rawValue: self.user.token]
        
        apiDelegate.query(endpoint: APIManager.Endpoint.announcementGet, parameters: parameters) {
            [unowned self] (result: Result) in
            
            DispatchQueue.main.async {
                switch result {
                    case .Success(let data):
                        do {
                            let announcements = try self.announcementDelegate.createAnnouncements(with: data)
                            self.dataSource.provider.objects = announcements
                            
                            if self.refreshControl.isRefreshing {
                                self.refreshControl.endRefreshing()
                            }
                    
                            self.tableView.reloadData()
                            self.hasNoAnnouncements = false
                    
                        } catch let error {
                    
                            self.hasNoAnnouncements = true
                    
                            guard let error = error as? DisplayableError
                                else { print("Error type not handled at \(self) line 182"); return }
                    
                            self.alert.title = error.title
                            self.alert.message = error.localizedDescription
                    
                            self.present(alert: self.alert)
                        }
            
                    case .Error(let error):
                        guard let error = error as? NetworkingError else { print("Error at \(self) line 255"); return }
                        self.present(alert: self.alert, error: error)
                
                }
            }
        }
    }
}

// MARK: - Announcement Editing
extension AnnouncementVC: AnnouncementEditing {
    private enum Text: String {
        case editPost = "Edit Post"
        case deletePost = "Delete Post"
        case cancel = "Cancel"
    }
    
    func editAnnouncement(at indexPath: IndexPath) {
        
        let alertActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actions = [
            UIAlertAction(title: Text.editPost.rawValue, style: UIAlertActionStyle.default) {
                [unowned self] (_: UIAlertAction) in
                let postAnnouncementVC = PostAnnouncementVC()
                postAnnouncementVC.tokenDelegate = self
                postAnnouncementVC.tableDelegate = self
                postAnnouncementVC.announcement = self.dataSource.provider.objects[indexPath.row]
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(postAnnouncementVC, animated: true)
            },
            UIAlertAction(title: Text.deletePost.rawValue, style: UIAlertActionStyle.destructive) {
                [unowned self] (_: UIAlertAction) in
                let deletedAnnouncement = self.dataSource.provider.objects[indexPath.row]
                let parameters: [String: String] = [
                    JSONKey.id.rawValue: String(deletedAnnouncement.id),
                    JSONKey.token.rawValue: self.user.token
                ]
                
                self.apiDelegate.query(endpoint: APIManager.Endpoint.announcementDelete, parameters: parameters) { (result: Result) in
                    DispatchQueue.main.async {
                        
                        switch result {
                            case .Success:
                                self.dataSource.provider.objects.remove(at: indexPath.row)
                                self.dataSource.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
                            
                            case .Error(let error):
                                guard let error = error as? NetworkingError else { print("Error at \(self) line 254"); return }
                                self.present(alert: self.alert, error: error)
                        }
                    }
                }
            },
            UIAlertAction(title: Text.cancel.rawValue, style: UIAlertActionStyle.cancel) {
                [unowned self] (_: UIAlertAction) in
                
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        ]
        
        for action in actions {
            alertActionSheet.addAction(action)
        }
        
        present(alertActionSheet, animated: true, completion: nil)
        
    }
}

// MARK: - Target Action Functions 
extension AnnouncementVC {
    @objc func postAnnouncementButtonPressed() {
        let postAnnouncementVC = PostAnnouncementVC()
        postAnnouncementVC.tokenDelegate = self
        postAnnouncementVC.tableDelegate = self
        tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(postAnnouncementVC, animated: true)
    }
    
    @objc func refresh() {
        self.getAnnouncements()
    }
}
