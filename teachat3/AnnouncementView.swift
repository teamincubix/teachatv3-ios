//
//  AnnouncementsView.swift
//  teachat3
//
//  Created by Julio Alorro on 9/20/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class AnnouncementView: UIView {
    // MARK: Text
    private enum Text: String {
        case navBarTitle = "Announcements"
        case noAnnouncements = "You currently have no announcements"
        case retrieve = "Retrieving announcements"
    }
    
    let refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.tintColor = TeachatUI.Color.lightGray
        control.alpha = 0.95
        let attributes = [
            NSFontAttributeName: UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 12.0)!,
            NSForegroundColorAttributeName: TeachatUI.Color.lightGrayFont
        ]
        control.attributedTitle = NSAttributedString(string: Text.retrieve.rawValue, attributes: attributes)
        return control
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.allowsSelection = true
        tableView.backgroundColor = UIColor.white
        return tableView
    }()
    
    let noAnnouncementLabel: UILabel = {
        let label = UILabel()
        label.text = Text.noAnnouncements.rawValue
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 14.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.white
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: - Navigation Item
    let buttonItem: UIBarButtonItem = {
        let button = UIBarButtonItem(
            image: TeachatUI.Image.post, style: UIBarButtonItemStyle.plain, target: nil, action: nil
        )
        return button
    }()
    
    let titleView: UIImageView = {
        let imageView = UIImageView(image: TeachatUI.Image.teachatNavbar)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.bounds = CGRect(x: 0.0, y: 0.0, width: 128.0, height: 22.0)
        return imageView
    }()
    
    // MARK: - Stored Properties
    private lazy var subviewsArray: [UIView] = [self.tableView, self.noAnnouncementLabel]
    
    // MARK: - Custom Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setSubviewsForAutoLayout(subviewsArray)
        tableView.setSubviewForAutoLayout(refreshControl)
        // MARK: Constraints
        tableView.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self)
            make.top.equalTo(self)
            make.trailing.equalTo(self)
            make.bottom.equalTo(self)
        }
        
        noAnnouncementLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.leading.equalTo(self)
            make.top.equalTo(self)
            make.trailing.equalTo(self)
            make.bottom.equalTo(self).offset(-49.0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

