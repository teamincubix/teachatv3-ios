//
//  ImageFetching.swift
//  teachat3
//
//  Created by Julio Alorro on 10/11/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol ImageFetching {
    
    func fetchImage(with string: String, completionHandler: @escaping (Result<Data>) -> Void)

}
