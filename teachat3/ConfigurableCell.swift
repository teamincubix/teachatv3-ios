//
//  ConfigurableCell.swift
//  teachat3
//
//  Created by Julio Alorro on 11/14/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol ConfigurableCell {
    
    associatedtype Object
    
    static var identifier: String { get }
    
    func configure(with: Object, at indexPath: IndexPath)

}
