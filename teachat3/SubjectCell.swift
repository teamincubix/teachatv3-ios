//
//  SubjectCell.swift
//  teachat3
//
//  Created by Julio Alorro on 10/12/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

class SubjectCell: UITableViewCell {
    
    // MARK: - Subviews
    fileprivate let subjectLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: 14.0)
        label.textColor = UIColor.darkGray
        return label
    }()
    
    // MARK: - Custom Initializer
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        tintColor = TeachatUI.Color.green
        contentView.setSubviewForAutoLayout(subjectLabel)
        
        subjectLabel.snp.remakeConstraints { [unowned self] (make: ConstraintMaker) in
            make.top.equalTo(self.contentView).offset(5.0)
            make.leading.equalTo(self.contentView).offset(15.0)
            make.trailing.equalTo(self.contentView).offset(-5.0)
            make.bottom.equalTo(self.contentView).offset(-5.0)
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Stored Properties
    var subject: Subject!
}

// MARK: - Public API
extension SubjectCell {
    func find(in subjects: [Subject]) {
        if subjects.index(of: subject) != nil {
            print("Checked")
            self.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            print("Unchecked")
        }
    }
    
    func configure(with subject: Subject) {
        self.subject = subject
        subjectLabel.text = self.subject.name
    }
}
