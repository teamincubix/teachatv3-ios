//
//  NetworkingError.swift
//  teachat3
//
//  Created by Julio Alorro on 11/7/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

enum NetworkingError: DisplayableError {
    case invalidURL(URLComponents)
    case noInternet(String)
    case errorJSON(String)
    case missingParameters(APIManager.Endpoint, String)
    
    var localizedDescription: String {
        switch self {
        case .invalidURL(let url):
            return url.string!
        case .noInternet(let string):
            return string
        case .errorJSON(let string):
            return string
        case .missingParameters(let endpoint, let parameter):
            return "Missing parameter(s): \(parameter) for \(endpoint) endpoint"
        }
    }
    
    var title: String {
        switch self {
        case .invalidURL:
            return "Invalid URL"
        case .noInternet:
            return "No Internet Connection"
        case .errorJSON:
            return "API Error"
        case .missingParameters:
            return "Missing Parameter(s)"
        }
    }
}
