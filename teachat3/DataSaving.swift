//
//  KeychainDelegate.swift
//  teachat3
//
//  Created by Julio Alorro on 9/19/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol DataSaving {
   
    func save(data: Saveable) -> Bool
    
}
