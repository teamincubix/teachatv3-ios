//
//  ForgotPasswordDelegate.swift
//  teachat3
//
//  Created by Julio Alorro on 9/20/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import Foundation

protocol ForgotPasswordDelegate {
    
    func checkResponse(in jsonData: [String: Any]?) -> Bool

}
