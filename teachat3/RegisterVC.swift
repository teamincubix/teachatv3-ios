//
//  RegistrationVC.swift
//  teachat3
//
//  Created by Julio Alorro on 9/21/16.
//  Copyright © 2016 Incubix Technologies, Inc. All rights reserved.
//

import UIKit
import SnapKit

final class RegisterVC: UIViewController, InteractionHandling, AlertHandling, TargetActionSetting {
    
    // MARK: Delegate Properties
    let apiDelegate: APIQuerying
    fileprivate let locationDelegate: LocationCreating
    
    // MARK: Custom Initializer
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        // RegisterVC should not know that APIManager is of type APIDelegate
        // Fix in the future when a solution is thought of
        self.apiDelegate = APIManager()
        self.locationDelegate = JSONManager()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = RegisterView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewController()
        self.setUpTextFieldDelegates()
        self.setUpPickers()
        self.registerForKeyboardNotifications()
        self.setUpTargetActions([
            self.roleSegmentControl: #selector(roleControlPressed),
            self.loginButton: #selector(loginButtonPressed),
            self.registerButton: #selector(registerButtonPressed),
        ])
        self.scrollView.delegate = self
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        self.apiDelegate.query(endpoint: APIManager.Endpoint.location, parameters: nil) { [unowned self]
            (result: Result) in
            
            DispatchQueue.main.async {
                switch result {
                    case .Success(let data):
                
                        do {
                        
                            self.locations = try self.locationDelegate.createLocation(with: data)
                        
                            guard let locations = self.locations,
                                let country = locations.countries.first,
                                let state = locations.states[country]?.first,
                                let school = locations.schools[state]?.first
                        
                            else {
                                fatalError("Something went wrong with RegisterVC's initial setup of locations")
                            }
                        
                            self.pickedCountry = school.state.country
                            self.pickedState = school.state
                            self.pickedSchool = school
                        
                            self.countryTextField.text = school.state.country.name
                            self.stateTextField.text = school.state.name
                            self.schoolTextField.text = school.name
                        
                        } catch let error {
                        
                            guard let error = error as? DisplayableError
                                else { print("Error type not handled at \(self) at line 78"); return }
                        
                            self.alert.title = error.title
                            self.alert.message = error.localizedDescription
                        
                            self.present(alert: self.alert)
                        }
                
                    case .Error(let error):
                        guard let error = error as? NetworkingError else { print("Error at \(self) line 87"); return }
                        self.present(alert: self.alert, error: error, action: self.dismissVCAction)
                
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        AnimationManager.animator.animateConstraintsOnScreen(
//            constraints: [self.centerLogoConstraint],
//            delay: 1,
//            position: AnimationManager.OffScreenPosition.left
//        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        AnimationManager.animator.removeAnimations([self.centerLogoConstraint])
    }
    
    deinit {
        print("\(self) was deallocated")
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Stored Properties
    fileprivate var locations: Location?
    fileprivate weak var activeTextField: UITextField?
    
    fileprivate var pickedCountry: Country?
    fileprivate var pickedState: State?
    fileprivate var pickedSchool: School?
    
    fileprivate lazy var dismissVCAction: UIAlertAction = { [unowned self] in
        let dismissAction = UIAlertAction(title: "Go Back", style: .default) { [unowned self]
            (_: UIAlertAction) in
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            self.loginButtonPressed()
        }
        return dismissAction
    }()
    
    fileprivate lazy var alert: UIAlertController = {
        return UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    }()
    
    fileprivate lazy var teacherTextFields: [TeachatTextField] = [
        self.countryTextField,
        self.stateTextField,
        self.schoolTextField,
    ]
    
    fileprivate lazy var textFields: [TeachatTextField] = [
        self.countryTextField, self.stateTextField,
        self.schoolTextField, self.firstNameTextField,
        self.lastNameTextField, self.emailTextField,
        self.passwordTextField, self.confirmPasswordTextField,
    ]
    
    fileprivate lazy var pickers: [UIPickerView] = [
        self.countryPicker,
        self.statePicker,
        self.schoolPicker
    ]

    // MARK: - teacherIsSelected Stored Property with a Property Observer
    fileprivate var teacherIsSelected: Bool = true {
        didSet {
            for textField in self.teacherTextFields {
                textField.isHidden = !self.teacherIsSelected
            }
            
            self.registerView.teacherIsSelected = teacherIsSelected
        }
    }
}

// MARK: - Computed Properties (and IBOutlets)
extension RegisterVC {
    fileprivate weak var registerView: RegisterView! { return self.view as! RegisterView }
    weak var roleSegmentControl: UISegmentedControl! { return self.registerView.roleSegmentControl }
    weak var titleSegmentControl: UISegmentedControl! { return self.registerView.titleSegmentControl }
    weak var countryTextField: TeachatTextField! { return self.registerView.countryTextField }
    fileprivate weak var countryPicker: UIPickerView! { return self.registerView.countryPicker }
    weak var stateTextField: TeachatTextField! { return self.registerView.stateTextField }
    fileprivate weak var statePicker: UIPickerView! { return self.registerView.statePicker }
    weak var schoolTextField: TeachatTextField! { return self.registerView.schoolTextField }
    fileprivate weak var schoolPicker: UIPickerView! { return self.registerView.schoolPicker }
    weak var firstNameTextField: TeachatTextField! { return self.registerView.firstNameTextField }
    weak var lastNameTextField: TeachatTextField! { return self.registerView.lastNameTextField }
    weak var emailTextField: TeachatTextField! { return self.registerView.emailTextField }
    weak var passwordTextField: TeachatTextField! { return self.registerView.passwordTextField }
    weak var confirmPasswordTextField: TeachatTextField! { return self.registerView.confirmPasswordTextField }
    weak var registerButton: UIButton! { return self.registerView.registerButton }
    weak var loginButton: UIButton! { return self.registerView.loginButton }
    fileprivate weak var centerLogoConstraint: Constraint! { return self.registerView.centerLogoConstraint }
    fileprivate weak var scrollView: UIScrollView! { return self.registerView.scrollView }
    fileprivate weak var activityIndicator: UIActivityIndicatorView! { return self.registerView.activityIndicator }
//    weak var scrollContentView: UIView! { return self.registerView.scrollContentView }
    
    fileprivate var registrationFormFilled: Bool {
        
        guard let firstName = firstNameTextField.text, !firstName.isEmpty,
            let lastName = lastNameTextField.text, !lastName.isEmpty,
            let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty,
            let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmpty
        else {
            return false
        }
        
        return true
        
    }
}

// MARK: - Helper Functions
fileprivate extension RegisterVC {
    
    func setUpViewController() {
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }
    
    func setUpTextFieldDelegates() {
        for textField in self.textFields {
            textField.delegate = self
        }
    }
    
    func setUpPickers() {
        for picker in self.pickers {
            picker.delegate = self
            picker.dataSource = self
        }
    }
    
    @objc func keyboardWasShown(_ notification: Notification) {
        self.changeUserInteraction(of: [self.loginButton], to: false)
        
        //Apple Way
        if let keyboardNSValue = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue {
            
            let keyboardSize = keyboardNSValue.cgRectValue.size
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            
            var rect = self.view.frame
            rect.size.height -= keyboardSize.height
            
            if let activeTextField = self.activeTextField {
            
                if !rect.contains(activeTextField.frame.origin) {
                    
                    self.scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
                
                }
            }
        }
    }
    
    @objc func keyboardWasDismissed(_ notification: Notification) {
        self.changeUserInteraction(of: [self.loginButton], to: true)
        
        let contentInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasDismissed(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    func getLocationArray(of country: Country?) -> [State] {
        
        guard let countryKey = country, let states = self.locations?.states[countryKey]
            else {print("State Array is empty"); return [] }
        
        return states
    }
    
    func getLocationArray(of state: State?) -> [School] {
        guard let stateKey = state, let schools = self.locations?.schools[stateKey]
            else {print("School Array is empty"); return [] }
        
        return schools
    }
    
}

// MARK: - Textfield Delegate Functions
extension RegisterVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.activeTextField = textField
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    
        self.activeTextField = nil
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let textField = textField as? TeachatTextField
            else { print("Could not typecast textfield"); return false }
        
        guard let lastTextField = self.textFields.last
            else { print("There are no elements in textFields array"); return false }
        
        if textField != lastTextField {
            
            guard let index = self.textFields.index(of: textField)
                else { print("textField is not in array"); return false }
            
            self.textFields[index + 1].becomeFirstResponder()
            
        } else if textField == lastTextField {
            textField.resignFirstResponder()
            
            if registrationFormFilled {
            
                self.registerButtonPressed()
            
            }
        }
        
        return false
    
    }
}

// MARK: - Picker Delegate Functions
extension RegisterVC: UIPickerViewDelegate {
    
    private enum ErrorPickerText: String {
        
        case noCountries = "No countries to display"
        case noStates = "No states/provinces to display"
        case noSchools = "No schools to display"
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView {
            case self.countryPicker:
                
                let country = self.locations?.countries[row]
                
                self.pickedCountry = country
                self.countryTextField.text = country?.name
                
                let states = getLocationArray(of: self.pickedCountry)
                
                self.pickedState = states.first
                
                let schools = getLocationArray(of: self.pickedState)
                
                self.pickedSchool = schools.first
                
                // Get the first element of state and school array
                self.stateTextField.text = self.pickedState?.name
                self.schoolTextField.text = self.pickedSchool?.name
            
            
            case self.statePicker:
            
                let states = getLocationArray(of: self.pickedCountry)
                guard !states.isEmpty else { return }
                
                let state = states[row]
                
                self.pickedState = state
                self.stateTextField.text = state.name
                
                let schools = getLocationArray(of: self.pickedState)
                self.pickedSchool = schools.first
                
                // Get the first element of school array
                self.schoolTextField.text = self.pickedSchool?.name
            
            case self.schoolPicker:
                let schools = getLocationArray(of: self.pickedState)
                guard !schools.isEmpty else { return }
            
                let school = schools[row]
                self.pickedSchool = school
                self.schoolTextField?.text = school.name
            
            default: print("Picker doesn't exist"); break
        }
        
        self.activeTextField?.resignFirstResponder()
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label: UILabel
        
        if let reusedView = view as? UILabel {
            
            label = reusedView
        
        } else {
            
            label = UILabel()
        
        }
        
        label.font = UIFont(name: TeachatUI.RobotoFont.regular.rawValue, size: TeachatUI.FontSize.textField)
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        
        let title: String?
        
        switch pickerView {
        
            case self.countryPicker:
                guard let countries = self.locations?.countries, !countries.isEmpty
                    else { title = ErrorPickerText.noCountries.rawValue; break }
                title = self.locations?.countries[row].name
            
            case self.statePicker:
            
                let states = getLocationArray(of: self.pickedCountry)
                guard !states.isEmpty else { title = ErrorPickerText.noStates.rawValue; break }
                title = states[row].name
        
            case self.schoolPicker:
                let schools = getLocationArray(of: self.pickedState)
                guard !schools.isEmpty else { title = ErrorPickerText.noSchools.rawValue; break }
                title = schools[row].name
            
            default: title = nil
        
        }
        
        label.text = title
        
        return label
        
    }
}

// MARK: - Picker Data Source Functions
extension RegisterVC: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
            case self.countryPicker:
                
                guard let countries = self.locations?.countries, !countries.isEmpty
                    else { return 1 }
                
                return countries.count
            
            case self.statePicker:
                
                let states = getLocationArray(of: self.pickedCountry)
                guard !states.isEmpty else { return 1 }
            
                return states.count
            
            case self.schoolPicker:
                
                let schools = getLocationArray(of: self.pickedState)
                guard !schools.isEmpty else { return 1 }
            
                return schools.count
            
            default: return 0
        }
    }
}

// MARK: - UIScrollViewDelegate Functions
extension RegisterVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.activeTextField?.resignFirstResponder()
    }
}

// MARK: - Target Functions (IBActions)
extension RegisterVC {
    
    // MARK: Title Enum
    fileprivate enum Title: String {
        case mr, ms, mrs
        
        init?(int: Int) {
            switch int {
                case 0: self = .mr
                case 1: self = .ms
                case 2: self = .mrs
                default: return nil
            }
        }
    }
    
    // MARK: - Alert Text Enum
    enum AlertText: String {
        case title = "Success!"
        case message = "Registration successful. Please check your email."
        case buttonMessage = "Ok"
    }
    // MARK: - Error Text Enum
    enum ErrorText: String {
        case missing = "You must fill all visible textfields first"
        case email = "Email is not valid"
        case mismatch = "Passwords do not match"
        case short = "Password is too short"
    }
    
    @objc func roleControlPressed() {
        // self.teacherIsSelected didSet observer will execute
        if self.roleSegmentControl.selectedSegmentIndex == 0 {
            self.teacherIsSelected = true
            
        } else if self.roleSegmentControl.selectedSegmentIndex == 1 {
            self.teacherIsSelected = false
        }
    }
    
    @objc func loginButtonPressed() {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func registerButtonPressed() {

        var parameters: [String: Any] = [:]
        var error: DisplayableError?
        
        // Executes if there is an error before exiting scope
        defer {
            if let error = error {
                present(alert: self.alert, error: error)
            }
        }
        
        if self.roleSegmentControl.selectedSegmentIndex == 0, let school = self.pickedSchool {
            
            parameters[JSONKey.schoolID.rawValue] = school.id
            parameters[JSONKey.countryID.rawValue] = school.state.country.id
            parameters[JSONKey.stateID.rawValue] = school.state.id
        
        }
        
        guard let title = Title(int: titleSegmentControl.selectedSegmentIndex),
            let firstName = self.firstNameTextField.text, !firstName.isEmpty,
            let lastName = self.lastNameTextField.text, !lastName.isEmpty,
            let email = self.emailTextField.text, !email.isEmpty,
            let password = self.passwordTextField.text, !password.isEmpty,
            let confirmPassword = self.confirmPasswordTextField.text, !confirmPassword.isEmpty
        else {
            error = TeachatError.missingInput(ErrorText.missing.rawValue); return
        }
            
        guard email.isValidEmail else { error = TeachatError.invalidInput(ErrorText.email.rawValue); return }
            
        guard password == confirmPassword else { error = TeachatError.invalidInput(ErrorText.mismatch.rawValue); return }
        
        guard password.characters.count >= 8 else { error = TeachatError.invalidInput(ErrorText.short.rawValue); return }
        
        parameters[JSONKey.roleID.rawValue] = self.roleSegmentControl.selectedSegmentIndex + 2 /* teacher role = 2, parent role = 3 */
        parameters[JSONKey.email.rawValue] = email
        parameters[JSONKey.password.rawValue] = password
        parameters[JSONKey.firstName.rawValue] = firstName
        parameters[JSONKey.lastName.rawValue] = lastName
        parameters[JSONKey.title.rawValue] = title.rawValue.capitalized
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        self.apiDelegate.query(endpoint: APIManager.Endpoint.registration, parameters: parameters) {
            [unowned self] (result: Result) in
            
            DispatchQueue.main.async {
                
                switch result {
                
                case .Success:
                
                    self.alert.title = AlertText.title.rawValue
                    self.alert.message = AlertText.message.rawValue
                
                    self.present(alert: self.alert, action: self.dismissVCAction)
                
                case .Error(let error):

                    guard let error = error as? NetworkingError else { print("Error at \(self) line 641"); return }
                    self.present(alert: self.alert, error: error)
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
            
            }
        }
    }
}
